const myLogin = require('./login.data.json');
const { SpecReporter } = require('jasmine-spec-reporter');
var ScreenShotReporter = require('protractor-jasmine2-screenshot-reporter');

let toTimestamp = (new Date()).toISOString().replace(/[^\d]/g,'').slice(0, -3);

exports.config = {
    framework: 'jasmine',
    allScriptsTimeout: 45000,
    // seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['./testscripts/scripts/*.spec.js',
            // './testscripts/scripts/form700u*Tests.spec.js',
            // './testscripts/scripts/orFormsMain*Tests.spec.js',
            // './testscripts/scripts/form800Tests.spec.js'
    ],

    suites: {
        form700: './testscripts/scripts/form700/*.spec.js',
        form800: './testscripts/scripts/form800/*.spec.js',
        formPHS: ['testscripts/scripts/FormPHS/FormPHSErrorMessageVerificationTests.spec.js',
                'testscripts/scripts/FormPHS/formPHSUIVerificationTests.spec.js',
                'testscripts/scripts/FormPHS/formPHSFilingTests.spec.js',
                'testscripts/scripts/FormPHS/formPHSSfiDeleteTests.spec.js',
        ],
        full: './**/*.spec.js',
    },


    capabilities: {
        // browserName: 'chrome',
        browserName: 'firefox',
           'moz:firefoxOptions': {
           'binary': 'C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe',
           'args': ['--verbose']
       }

    },
    directConnect: true,
    // baseUrl: 'https://or-tc9-dev.ucdavis.edu/orforms-dev/',
    baseUrl: 'http://localhost:8080/orforms/',

    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000,
        print: function() {}
        },
    params: {
        login: {
            username: (myLogin.username),
            password: (myLogin.password)
        },

        SevenHundredInfo:['Required for PIs with each project funded or supported in whole or in part by by a non-governmental entity. This applies to both sponsored awards and gifts.'],
        // SevenHundredInfo:['Form 700-U is required for each project funded or supported in whole or in part by a non-governmental entity. A new form is required to be filed within 30 days of receipt of new funds.'],
        EightHunderedInfo:['Principal Investigators (and all other persons who have responsibility for the design, conduct or reporting of research) must disclose their personal and family member\'s financial interest.'],
        PhsInfo: ['Filing of a COI Form PHS is required annually. Principal Investigators and all persons who have responsibility for the design, conduct or reporting of research funded by Public Health Services (including NIH) are required to file this form.'],
        SupplementInfo: ['This form is required when a Principal Investigator and/or Other Investigator has made a positive disclosure of financial interest on the Form 800 or 700-U.'],
        AssociationHelp: ['Are you a director, officer, partner, trustee, consultant, employee, or do you hold a position of management in the entity listed in the Funding Details section?'],
        InvestmentHelp: ['Do you, your spouse, or registered domestic partner, or your dependent children have an investment of $2,000 or more in the entity listed in the Funding Details section?'],
        IncomeHelp: ['Have you received income of $500 or more from the entity listed in the Funding Details section during the reporting period?'],
        LoanInfoHelp: ['Have you received loans from the entity listed in the Funding Details section for which the balance exceeded $500 during the reporting period?'],
        RelatedGiftsHelp: ['Have you received gifts from the entity listed in the Funding Details section within the last 12 months valued at $50 or more?'],
        RelatedTravelHelp: ['Has the entity listed in the Funding Details section paid for your travel during the reporting period?'],
        VerificationSignatureStatement: ['I have used all reasonable diligence in preparing this statement. I have reviewed this statement and to the best of my knowledge the information contained herein and in any attached schedules is true and complete. I certify under penalty of perjury under the laws of the State of California that the foregoing is true and correct.'],
        SFISupplementalMessage: ['Based on your responses you are required to complete a supplemental Statement of Economic Interest.'],
        Form800TableHeaders: ['Investigator', 'Signature', 'Budget Period Start', 'Budget Period End', 'Date Submitted', 'Project', 'Funding Entity', 'Status', 'Supplemental Form', 'PDF'],
        Form800PPM230Message: ['Under UC Davis PPM 230-05, the Principal Investigator (PI) and other Investigators (all persons who have responsibility) for the design, conduct or reporting of research) must disclose ALL their personal and family member\'s SIGNIFICANT FINANCIAL INTERESTS ("SFI") in any entity or organization that may have a significant impact on the conduct of the research or that might benefit from the anticipated results of the proposed project.'],
        Form800SFIMessage: ['An SFI, as defined in PPM 230-05.II.G, includes anything of significant monetary value, including but not limited to salary or other payments for services; equity interests (e.g., stocks, stock options or other ownership interests); intellectual property rights (e.g., patents, copyrights and royalties from such rights); or holding a position as an officer, director, agent, or employee of a business entity. Please note that an SFI includes not only a financial interest in the sponsor of your research, but ALSO any SFI you may have in any other outside business entity, such as a competitor of the sponsor or other entities that my have an interest in the products, testing, or services that are part of your research.'],
        Form800SFIDisclosureText: ['Do you, your spouse, registered domestic partner, or dependent children have any \'Significant Financial Interests\' (as defined in PPM 230-05.II.G) RELATED to the work to be conducted under the proposed project that was received within the last twelve months or that you expect to receive in the next twelve months?'],
        Form800VerificationText: ['I acknowledge and certify: (1) my responsibility to immediately disclose any new reportable financial interest obtained during the term of the project, and (2) all other investigators, who will have the responsibility for the design, conduct or reporting of research will submit the Form 800, and (3) this is a complete disclosure of my financial interests related to the proposed project/sponsor.'],
        FormPHSGlossary: ['Under federal law promulgated by the Public Health Service (PHS) and UC Davis PPM 230-07, the Principal Investigator (PI) and all other covered "Investigators" must disclose certain defined personal (or individual) financial interests that exceed the thresholds provided by PHS and that are related to their INSTITUTIONAL RESPONSIBILITIES. Such disclosed financial interests, which also include interests held by an investigator\'s spouse/registered domestic partner and dependent children (collectively referred to as "immediate family members" in this disclosure), are deemed SIGNIFICANT FINANCIAL INTERESTS (SFI). Separate disclosures of SFI(s) must be made by every person who meets the definition of an "Investigator," including the Principal Investigator, and all Co-Investigators, Senior/Key Personnel, Collaborators, Consultants, and any other individual who, regardless of title or position, has responsibility for the design, conduct, or reporting of research that is funded, or proposed for funding, by any PHS agency or any other sponsor that has adopted the PHS regulations ("COVERED ENTITY").'],
        FormPHSTrainingInfo: ['All INVESTIGATORS (as defined in the regulations) must complete this training prior to beginning work on any project that is funded by any PHS agencies or any other sponsor that has adopted the PHS regulations ("COVERED ENTITY") with a Notice of Award issue date after August 24th, 2012. To complete training:'],
        FormPHSTrainingList: ['Log in to the UC Learning Center using your UCD LoginID and Kerberos password.\nSearch for keyword "COIC" in the UC Learning Center search bar on the left of your screen.\nClick "Start" for the Conflict of Interest class. The online course will then launch.'],
        FormPHSIncomeGlossary: ['Enter any INCOME you (or any IMMEDIATE FAMILY MEMBER) received from TBD during the last 12 months related to your INSTITUTIONAL RESPONSIBILITIES.'],
        FormPHSEquityGlossary: ['Do you (or any IMMEDIATE FAMILY MEMBER) hold any EQUITY INTEREST in TBD related to your INSTITUTIONAL RESPONSIBILITIES as of the date of this disclosure?'],
        FormPHSIPGlossary: ['In the past 12 months, have you or any IMMEDIATE FAMILY MEMBER received any payments for any INTELLECTUAL PROPERTY RIGHTS AND INTERESTS related to your INSTITUTIONAL RESPONSIBILITIES that, when totalled, exceeds $5,000? (This includes, for example patents or copyrights, assigned or licensed to a party other than The Regents)?'],
        FormPHSIPNote: ['Note: Only income from patents or another intellectual property received from any organization that exceeds $5,000 during the prior 12 months must be disclosed. Do not include royalties received from The Regents of the University of California related to patents or copyrights.'],
        FormPHSTravelGlossary: ['At any time in the last 12 months, have you undertaken any travel related to your INSTITUTIONAL RESPONSIBILITIES that was sponsored by any ENTITY, or for which you have received any TRAVEL REIMBURSEMENT OR PAYMENTS from any ENTITY? (This does not include sponsorship, reimbursement, or payments from federal, state, or local government, a U.S. institution of higher education, or a research institute, academic medical center or hospital affiliated with an institution of higher education.)?'],
        FormPHSTravelNote: ['Note: Only travel reimbursement that exceeds $5,000 per entity (when aggregated) during the prior 12 months must be disclosed.'],
        FormPHSOutsideActivityGlossary: ['Other than what you have already disclosed, do you or any IMMEDIATE FAMILY MEMBER hold any position or have decision making authority with any entity (e.g. Director, Partner, Member Board of Directors, Member Scientific Advisory Board, Officer) where such position is in any way RELATED to any of your research funded by a COVERED ENTITY?'],
        FormPHSSignatureAcknowledement:['I certify that this is a complete disclosure of all my SIGNIFICANT FINANCIAL INTERESTS (SFIs). I acknowledge that by typing my name below it is my responsibility to update this disclosure a minimum of annually, and within 30 days after the acquisition/discovery of any new SFIs.'],
        FormPHSIncomeModalIncomeGlossary:['INCOME related to your INSTITUTIONAL RESPONSIBILITIES received by you or an IMMEDIATE FAMILY MEMBER in TEST within the last 12 months:'],
        FormPHSIncomeModalEqutiyGlossary:['EQUITY INTEREST related to your INSTITUTIONAL RESPONSIBILITIES held by you or an IMMEDIATE FAMILY MEMBER in TEST within the last 12 months:'],
        FormPHSIncomeModalReasonableGlossary: ['In your opinion, could a reasonable person perceive that this SFI is related to any of your research that is funded by a PHS Agency (e.g. NIH) and/or any other COVERED ENTITY?'],
        FormPHSIncomeModalPublicStatementGlossary: ['Provide a separate statement explaining your answer above regarding why the financial interest either is or is not related to each of your PHS funded projects and/or any other COVERED ENTITY funded research (include Cayuse #(s) as applicable).'],
        FormPHSIncomeModalNPResponsabilitiesGlossary: ['INCOME related to your INSTITUTIONAL RESPONSIBILITIES received by you or an IMMEDIATE FAMILY MEMBER in TEST within the last 12 months:'],
        FormPHSIPModalTotalAmountRange: ['Select\n$5,001 - $9,999\n$10,000 - $19,999\n$20,000 - $39,999\n$40,000 - $59,999\n$60,000 - $79,999\n$80,000 - $99,999\n$100,000 - $149,999\n$150,000 - $199,999\n$200,000 and above'],
        FormPHSIPModalRelationshipList: ['Select\nSelf\nSpouse/Registered Domestic Partner\nDependent Children\nSelf + Immediate Family Member(s)'],
        FormPHSIPModalRelationshipGlossary: ['Please provide a description of the intellectual property from the COVERED ENTITY.'],
        FormPHSIPModalOpinionGlossary: ['In your opinion, could a reasonable person perceive that this SFI is related to any of your research that is funded by a PHS Agency (e.g. NIH) and/or any other COVERED ENTITY?'],
        FormPHSIPModalStatementGlossary: ['Provide a separate statement explaining your answer above regarding why the financial interest either is or is not related to each of your PHS funded projects and/or any other COVERED ENTITY funded research (include Cayuse #(s) as applicable).'],
        FormPHSTravelModalNote: ['Note: The regulations require travel Disclosures to be updated within 30 days after travel occurs. To avoid multiple future updates to this Disclosure you may (but are not required to) prospectively report travel for the upcoming 12 months as part of this Disclosure. If, however, your travel plans change from what you disclose at this time, you must submit an updated Disclosure within 30 days of such change.'],
        FormPHSTravelModal5KNote: ['Only travel reimbursement that exceeds $5,000 per entity (when aggregated) during the prior 12 months must be disclosed.'],
        FormTravelModalReasonableQuestGlosssary: ['In your opinion, could a reasonable person perceive that this SFI is related to any of your research that is funded by a PHS Agency (e.g. NIH) and/or any other COVERED ENTITY?'],
        FormTravelModalStatementGlossary: ['Provide a separate statement explaining your answer above regarding why the financial interest either is or is not related to each of your PHS funded projects and/or any other COVERED ENTITY funded research (include Cayuse #(s) as applicable).'],
        FormPHSManagementPositionList:['Select\nDirector\nPartner\nMember Board of Directors\nMember Scientific Advisory Board\nOfficer\nTrustee\nOther'],
        FormPHSManagementEntityGlossary:['Identify the ENTITY and describe you and/or your IMMEDIATE FAMILY MEMBER\'S responsibilities related to the position(s):'],
    },

    onPrepare() {
        jasmine.getEnv().addReporter(new ScreenShotReporter({
            dest: 'screenshots/',
            filename: 'test_report_' + toTimestamp + '.html',
            showSummary: true,
            showQuickLinks: true,
            preserveDirectory: true,
            reportOnlyFailedSpecs: true,
            reportFailedUrl: true,
            captureOnlyFailedSpecs: true
         }));

        browser.ignoreSynchronization = true;
        browser.driver.get(browser.baseUrl);
        browser.driver.findElement(by.id('username')).sendKeys(browser.params.login.username);
        browser.driver.findElement(by.id('password')).sendKeys(browser.params.login.password);
        element(by.id('submit')).click();
        browser.sleep(3000);

        const width = 1500;
        const height = 800;
        browser.driver.manage().window().setPosition(0, 0);
        browser.driver.manage().window().setSize(width, height);

        jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    }
}

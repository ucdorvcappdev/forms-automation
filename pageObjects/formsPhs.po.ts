import { browser, by, element, $, $$, protractor } from 'protractor';

export class FormPhsPage {
    navigateTo() {
        return (browser.get(browser.baseUrl + 'landing?tab=formPHS'));
    }

    getPhsUrlText(){
        return (browser.getCurrentUrl());
    }

    getFormPhsText() {
        return $('.automation-formPhs').getText();
    }

    getPhsTableHeaderText() {
        return $('.automation-phs-table-headers').getText();
    }

    isPhsNoFormSubmittedDisplayed() {
        return $('.automation-no-phs').isPresent();
    }

    getPhsCreateFormLinkText() {
        return $('.automation-phs-create-form').getText();
    }

    getPhsHeaderText() {
        return $('.automation-phs-header').getText();
    }

    getPhsGlossaryText() {
        return $('.automation-phs-glossary').getText();
    }

    getPhsGlossaryFormLinkText() {
        return $('a.automation-phs-glossary-link').getText();
    }

    getPhsInvestigatorInfoLabel() {
        return $('.automation-phs-investigator-info-label').getText();
    }

    getPhsNameLabel() {
        return $('.automation-phs-name-label').getText();
    }

    getPhsInvestigatorText() {
        return $('.automation-phs-name').getText();
    }

    getPhsEmailLabel() {
        return $('.automation-phs-email-label').getText();
    }

    getPhsEmailText() {
        return $('.automation-phs-email').getText();
    }

    getPhsDepartmentLabel() {
        return $('.automation-phs-dept-label').getText();
    }

    getPhsDepartmentText() {
        return $('.automation-phs-department').getText();
    }

    getPhsCertificationLabel() {
        return $('.automation-phs-certification-label').getText();
    }

    getPhsTrainedQuestionText() {
        return $('.automation-phs-trained-question').getText();
    }

    getPhsTrainingInfoText() {
        return $('.automation-phs-training-info').getText();
    }

    isCompletedTrainingOptionsDisplayed() {
        return $('.automation-phs-trained-options').isDisplayed();
    }

    getPhsTrainingText() {
        return $('.automation-phs-training').getText();
    }

    isPhsTrainingInfoTextDisplayed() {
        return $('.automation-phs-training-info').isDisplayed()
    }

    getPhsTrainingListText() {
        return $('.automation-phs-training-list').getText();
    }

    getPhsDisclosureLabel() {
        return $('.automation-phs-disclosure-label').getText();
    }

    getPhsDisclosureReasonLabel() {
        return $('.automation-phs-disclosure-reason-label').getText();
    }

    isPhsDisclosureReasonOptionsDisplayed() {
        return $('.automation-phs-reason-radios').isDisplayed();
    }

    getPhsDisclosureReasonOptionsText() {
        return $('.automation-phs-reason-radios').getText();
    }

    getPhsSection4Label() {
        return $('.automation-phs-section4-label').getText();
    }

    getPhsWizardLabel() {
        return $('.automation-phs-sfi-wizard-label').getText();
    }

    getPhsIncomeNoteText() {
        return $('.automation-phs-income-note').getText();
    }

    getPhsWizardEntityLabel() {
        return $('.automation-phs-entity-dropdown-label').getText();
    }

    getPhsWizardEntityTypeLabel() {
        return $('.automation-phs-entityType-label').getText();
    }

    getPhsIncomeGlossaryText(){
        return $('.automation-phs-income-glossary').getText()
    }
    getPhsEquityGlossaryText(){
        return $('.automation-phs-equity-glossary').getText()
    }

    getPhsIncomeTextboxPlaceholder() {
        return $('.automation-phs-wizard-income').getAttribute('placeholder')
    }

    getPhsAddSfiButtonText() {
        return $('.automation-phs-add-income-sfi-btn').getText();
    }

    getPhsSfiTableHeader() {
        return $('.automation-phs-sfi-table-header').getText();
    }

    getPhsNoSfiReportedMessage() {
        return $('.automation-phs-no-sfi-reported-message').getText();
    }

    getPhsEntityDropDownText() {
        return $('.automation-phs-entity-list').getText();
    }

    getPhsNewEntityTextboxPlaceholderText() {
        return $('.automation-phs-new-entity-textbox').getAttribute('placeholder');
    }

    getPhsEntityTypeDropDownText() {
        return $('.automation-phs-entityType-list').getText();
    }

    isPhsDraftDisplayed() {
        return $('.automation-phs-draft').isDisplayed();
    }

    getPhsSection5Label() {
        return $('.automation-phs-section5-label').getText();
    }

    getPhsIPGlossaryText() {
        return $('.automation-phs-ip-glossary').getText();
    }

    getPhsIPNoteText(){
        return $('.automation-phs-ip-note').getText();
    }

    getPhsIPTableHeaderText() {
        return $('.automation-phs-ip-table-header').getText();
    }

    getPhsTravelSectionLabel(){
        return $('.automation-phs-travel-label').getText();
    }

    getPhsTravelGlossaryText(){
        return $('.automation-travel-glossary').getText();
    }

    getPhsTravelNoteText() {
        return $('.automation-travel-note-text').getText();
    }

    getPhsTravelTableHeader() {
        return $('.automation-phs-travel-table-header').getText();
    }

    getPhsOutsideActivitySectionLabel() {
        return $('.automation-phs-outside-activity-label').getText();
    }

    getPhsOutsideActivityGlossaryText() {
        return $('.automation-phs-management-glossary').getText();
    }

    getPhsManagementTableHeader(){
        return $('.automation-phs-management-table-header').getText();
    }

    getPhsS8ResearchProjectLabel() {
        return $('.automation-phs-section8-label').getText();
    }

    getPhsResearchProjectLabel(){
        return $('.automation-phs-research-project-label').getText();
    }

    getPhsResearchProjectTableHeader(){
        return $('.automation-phs-project-table-header').getText();
    }

    getPhsManageProjectLinkText(){
        return $('a.automation-phs-manage-proj-link').getText();
    }

    getPhsS9SignatureLabel(){
        return $('.automation-phs-section9-signature-label').getText();
    }

    getPhsSignatureGlossaryText(){
        return $('.automation-phs-glossary-text').getText();
    }

    getPhsSignatureTextboxLabel(){
        return $('.automation-phs-signature-label').getText();
    }

    getPhsOutsideActivity(){
        return $('').getText();
    }

    // Income Modal functions
    getPhsModalIncomeEntitylabel() {
        return $('.automation-phs-modal-income-title').getText();
    }

    getPhsModalIncomeEntityTitleText() {
        return $('.automation-phs-income-modal-entity-label').getText();
    }

    getPhsModalIncomeEntityTypeTitleText() {``
        return $('automation-phs-income-modal-entityType-label').getText();
    }

    getPhsModalIncomeEntityValue(){
        return $('.automation-phs-incomeModal-entity-value').getText();
    }

    getPhsModalIncomeEntityTypeValue(){
        return $('.automation-phs-incomeModal-entityType-value').getText();
    }

    getPhsModalIncomeGlossary(){
        return $('.automation-phs-incomeModal-income-glossary').getText();
    }

    getPhsModalIncomeValue(){
        return $('.automation-phs-incomeModal-income').getText();
    }

    getPhsModalIncomeEquityGlossary(){
        return $('.automation-phs-incomeModal-equity-glossary').getText();
    }

    getPhsModalIncomeEquityValue(){
        return $('.automation-phs-incomeModal-equity').getText();
    }

    getPhsModalIncomeStatementTypeLabel() {
        return $('.automation-phs-incomeModal-statementType-label').getText();
    }

    getPhsModalIncomeRelationshipOptionsText() {
        return $$('.automation-phs-incomeModal-relationship-select').getText();
    }

    getPhsModalIncomePurposeLabel() {
        return $('.automation-phs-incomeModal-purpose-label').getText();
    }

    getPhsModalIncomeTableHeaders(){
        return $('.automation-phs-incomeModal-table-label').getText();
    }

    getPhsModalIncomeDollarsHelpText() {
        return $('.autoamtion-phs-incomeModal-total-dollars-help').getText();
    }

    getPhsModalIncomePurposeGlossary() {
        return $('.automation-phs-incomeModal-purpose-glossary').getText();
    }

    getPhsModalIncomeReasonableGlossary() {
        return $('.automation-phs-incomeModal-reasonable-glossary').getText();
    }

    getPhsModalIncomeStatementGlossary() {
        return $('.automation-phs-incomeModal-pub-statement-glossary').getText();
    }

    isPhsIncomeModalCancelButtonDisplayed() {
        return $('.automation-phs-incomeModal-cancel-btn').isDisplayed();
    }

    isPhsIncomeModalSubmitButtonDisplayed() {
        return $('.automation-phs-incomeModal-submit-btn').isDisplayed();
    }

    isPhsIncomeModalDeleteButtonDisplayed() {
        return $('.automation-phs-incomeModal-delete-btn').isDisplayed();
    }

    isPhsIncomeModalConfirmDeleteButtonDisplayed() {
        return $('.automation-phs-incomeModal-confirmDelete-btn').isDisplayed();
    }

    selectPhsEntity(sponsor) {
        return $('#otherEntity.automation-phs-entity-list')
            .element(by.cssContainingText('option',sponsor)).click();
    }

    selectPhsEntityTypeList(entityType) {
        return $('#entityType.automation-phs-entityType-list')
            .element(by.cssContainingText('option',entityType)).click();
    }

    clickPhsAddIncomeSfiButton() {
        return $('.automation-phs-add-income-sfi-btn').click();
    }


    setPhsIncomeAmount(myAmount) {
        return $('.automation-phs-wizard-income').sendKeys(myAmount)
    }

    clickPhsAddSfi(addSFI) {
        return $$('.radio').then(function(options) {
            switch(addSFI) {
                case 'ipRights':
                    return $('.automation-phs-add-ip-sfi-btn').click();
                case 'travel':
                    return $('.automation-phs-add-sfi-travel-button').click();
                case 'management':
                    return $('.automation-phs-management-button').click();
            }
        });
    }

    startPhsFiling() {
        return $('.automation-phs-draft').isPresent().then(function(draft){
            if ( draft === true) {
                return $('.automation-phs-draft').click();
            }
            else
                $('.automation-phs-create-form').click();
        });
    }

    selectPhsYesNoOption(myCss, option) {
        return $$('.radio').then(function(options) {
            // browser.sleep(200);
            if (option === 'YES') {
                browser.sleep(200);
                return $('#' + myCss + '1').click();
            } else {
                browser.sleep(200);
                return $('#' + myCss + '2').click();
            }
        });
    }

    selectPhsDisclosureType(disclosureType) {
        return $$('.radio').then(function(options) {
            switch (disclosureType) {
                case 'Initial':
                    return $$('#statementType1').click();
                case 'Annual':
                    return $$('#statementType2').click();
                case 'New Project':
                    return $$('#statementType3').click();
                case 'Correction':
                    return $$('#statementType4').click();
            }
        });

    }

    setPhsSignature() {
        return $('.automation-phs-signature-textbox')
            .sendKeys('Signed by Protrator Automation' + protractor.Key.ENTER);
    }

    clickPhsCancelButton() {
        return $('.automation-phs-cancel-btn').click();
    }

    clickPhsSubmitButton() {
        return $('.automation-phs-submit-btn').click();
    }

    clickPhsSaveDraftButton() {
        return $('.automation-phs-saveDraft-btn').click();
    }

    clickPhsIncomeModalSubmitButton() {
        return $('.automation-phs-incomeModal-submit-btn').click();
    }

    clickPhsIncomeModalNPSubmitButton() {
        return $('.automation-phs-incomeModal-np-submit-btn').click();
    }

    clickPhsIpModalSubmitButton() {
        var submitButton = $('.automation-phs-ipModal-submit-btn');
        submitButton.click();
        browser.sleep(1300);
        this.getPhsHeaderText();
        return;
    }

    clickPhsIpModalSubmitAgainButton() {
        return $('.automation-phs-ipModal-submit-repeat-btn').click();
    }

    clickPhsIncomeModalCancelButton() {
        return $('.automation-phs-incomeModal-cancel-btn').click();
    }

    clickPhsIncomeModalNpCancelButton() {
        return $('.automation-phs-incomeModal-np-cancel-btn').click();
    }


    // Intellectual Property SFI Modal

    getPhsIPModalTitleText(){
        return $('.automation-phs-ip-modal-title-label').getText();
    }

    getPhsIPModalEntityLabel(){
        return $('.automation-phs-ipModal-entity-label').getText();
    }

    getPhsIpModalEntiySelectList(){
        return $('.automation-phs-ipModal-entiy-select-list').getText();
    }

    getPhsIpModalTotalAmountLabel() {
        return $('.automation-phs-ipModal-amount-label').getText();
    }

    getPhsIpModalTotalAmountSelectListText() {
        return $('.automation-phs-ipModal-amount-select').getText();
    }

    getPhsIpModalRelationshipLabel() {
        return $('.automation-phs-ipModal-relationship-label').getText();
    }

    getPhsIpModalRelationshipSelectListText() {
        return $('.automation-phs-ipModal-relationship-select').getText();
    }

    getPhsIpModalIpDescriptionGlossary() {
        return $('.automation-phs-ipModal-ip-description').getText();
    }

    getPhsIpModalOpinionGlossary() {
        return $('.automation-phs-ipModal-opinion-glossary').getText();
    }

    getPhsIpModalStatementGlossary() {
        return $('.automation-phs-ipModal-statement-glossary').getText();
    }

    setPhsIpModalStatementTextbox() {
        return $$('.automation-phs-ipModal-statement-textbox').sendKeys('TEST DATA -');
    }

    isPhsIpModalCancelButtonDisplayed() {
        return $('.automation-phs-ipModal-cancel-btn').isDisplayed();
    }

    isPhsIpModalSubmitButtonDisplayed() {
        return $('.automation-phs-ipModal-submit-btn').isDisplayed();
    }

    isPhsIpModalSubmitAgainButtonDisplayed() {
        return $('.automation-phs-ipModal-submit-repeat-btn').isDisplayed();
    }

    isPhsIpModalDeleteButtonDisplayed() {
        return $('.automation-phs-ipModal-delete-btn').isDisplayed();
    }

    isPhsIpModalDeleteConfirmButtonDisplayed() {
        return $('.automation-phs-ipModal-delete-confirm-btn').isDisplayed();
    }

    // Travel Modal
    getPhsTravelModalTitleText() {
        return $('.automation-phs-travelModal-title').getText();
    }

    getPhsTravelModalTravelNoticeText() {
        return $('.automation-phs-travelModal-note').getText();
    }

    getPhsTravelModal5KNoteText() {
        return $('.automation-phs-travelModal-5k-note').getText();
    }

    getPhsTravelModalEntityLabel() {
        return $('.automation-phs-travelModal-entity-label').getText();
    }

    getPhsTravelModalEntitySelectListText() {
        return $('.automation-phs-travelModal-entity-select').getText();
    }

    getPhsTravelDateLabel(){
        return $('.automation-phs-travelModal-travel-beginEnd-label').getText();
    }

    getPhsTravelModalBeginTravelDate() {
        return $('.automation-phs-travelModal-travel-begin-input').getText();
    }

    getPhsTravelModalEndTravelDate() {
        $('.automation-phs-travelModal-travel-end-input').takeScreenshot();
        return $('.automation-phs-travelModal-travel-end-input').getText();
    }

    setPhsTravelModalBeginTravelDate(setStartDate) {
        return $('.automation-phs-travelModal-travel-begin-input')
            .sendKeys(setStartDate + protractor.Key.ENTER);
    }

    setPhsTravelModalEndTravelDate(setEndDate) {
        return $('.automation-phs-travelModal-travel-end-input')
            .sendKeys(setEndDate + protractor.Key.ENTER);
    }

    getPhsTravelModalTravelReasonQuestion() {
        return $('.automation-phs-travelModal-travel-reason-label').getText();
    }

    getPhsTravelModalReasonableGlossary() {
        return $('.automation-phs-travelModal-reasonable-question').getText();
    }

    getPhsTravelModalStatementGlossary() {
        return $('.automation-phs-travelModal-travel-statement-question').getText();
    }

    isPhsTravelModalCancelButtonDisplayed() {
        return $('.automation-phs-travelModal-cancel-btn').isDisplayed();
    }

    isPhsTravelModalSubmitButtonDisplayed() {
        return $('.automation-phs-travelModal-submit-btn').isDisplayed();
    }

    isPhsTravelModalSubmitAgainButtonDisplayed() {
        return $('.automation-phs-travelModal-submit-again-btn').isDisplayed();
    }

    isPhsTravelModalDeleteButtonDisplayed() {
        return $('.automation-phs-travelModal-delete-btn').isDisplayed();
    }

    isPhsTravelModalConfirmDeleteButtonDisplayed() {
        return $('.automation-phs-travelModal-deleteConfirm-btn').isDisplayed();
    }

    // Outside research position

    getPhsManagementModalSfiTitle() {
        return $('.automation-phs-manageModal-title').getText();
    }

    getPhsManagementModalPositionLabel() {
        return $('.automation-phs-manageModal-position-label').getText();
    }

    getPhsManagementModalPositionSelectListText() {
        return $$('.automation-phs-manageModal-position-select').getText();
    }

    getPhsManagementModalEntityGlossary() {
        return $('.automation-phs-manageModal-entity-glossary').getText();
    }

    isPhsManagementModalCancelButtonDisplayed() {
        return $('.automation-phs-manageModal-cancel-btn').isDisplayed();
    }

    isPhsManagementModalSubmitButtonDisplayed() {
        return $('.automation-phs-manageModal-submit-btn').isDisplayed();
    }

    isPhsManagementModalSubmitAnotherButtonDisplayed() {
        return $('.automation-phs-manageModal-submitAnother-btn').isDisplayed();
    }

    isPhsManagementModalDeleteButtonDisplayed() {
        return $('.automation-phs-manageModal-delete-btn').isDisplayed();
    }

    isPhsManagementModalDeleteConfirmDisplayed() {
        return $('.automation-phs-manageModal-delete-confirm-btn').isDisplayed();
    }

    // Non-publicly traded Income Modal

    getPhsIncomeModalNPTitle() {
        return $('.automation-phs-incomeModal-np-title').getText();
    }

    getPhsIncomeModalNPEntityLabel() {
        return $('.automation-phs-incomeModal-np-entity-label').getText();
    }

    getPhsIncomeModalNPEntityValue() {
        return $('.automation-phs-incomeModal-np-entity-value').getText();
    }

    getPhsIncomeModalNPEntityTypeLabel() {
        return $('.automation-phs-incomeModal-np-entityType-label').getText();
    }

    getPhsIncomeModalNPEntityTypeValue() {
        return $('.automation-phs-incomeModal-np-entityType-value').getText();
    }

    getPhsIncomeModalNPGlossary() {
        return $('.automation-phs-incomeModal-np-glossary').getText();
    }

    getPhsIncomeModalNPStatementTypeLabel() {
        return $('.automation-phs-IncomeModal-np-statementType-label').getText();
    }

    getPhsIncomeModalNPRelationshiptSelectList() {
        return $('.automation-phs-IncomeModal-np-relationship-select').getText();
    }

    getPhsIncomeModalNPPurposeLabel() {
        return $('.automation-phs-IncomeModal-np-purpose-label').getText();
    }

    getPhsIncomeModalNPItemizeTableHeaders() {
        return $('.automation-phs-IncomeModal-np-itemize-table-labels').getText();
    }

    getPhsIncomeModalNPTotalDollarsLabel() {
        return $('.automation-phs-incomeModal-np-dollars-help').getText();
    }

    getPhsIncomeModalNPReasonableQuestion() {
        return $('.automation-phs-incomeModal-np-reasonable-question').getText();
    }

    getPhsIncomeModalNPPurposeQuestion() {
        return $('.automation-phs-incomeModal-np-purpose').getText();
    }

    getPhsIncomeModalNPStatementQuestion() {
        return $('.automation-phs-incomeModal-np-statement-question').getText();
    }

    isPhsIncomeModalNPCancelButtonDisplayed() {
        return $('.automation-phs-incomeModal-np-cancel-btn').isDisplayed();
    }

    isPhsIncomeModalNPSubmitButtonDisplayed() {
        return $('.automation-phs-incomeModal-np-submit-btn').isDisplayed();
    }

    isPhsIncomeModalNPDeleteButtonDisplayed() {
        return $('.automation-phs-incomeModal-np-delete-btn').isDisplayed();
    }
    isPhsIncomeModalNPDeleteConfirmButtonDisplayed() {
        return $('.automation-phs-incomeModal-np-delete-confirm-btn').isDisplayed();
    }

    setPhsIncomeEquityInterestAmount(equityAmount) {
        return $('.automation-phs-equity-interest-amount').sendKeys(equityAmount);
    }

    //Main page Error Messages
    getPhsDocumentHasErrorMessage() {
        return $('.automation-phs-doc-error').getText();
    }

    getPhsStatementTypeErrorMessage() {
        return $('.automation-phs-statement-error').getText();
    }

    getPhsDocumentNotSavedErrorMessage() {
        return $('.automation-phs-doc-not-saved-error').getText();
    }

    getPhsIncomeIncompleteErrorMessage(){
        return $('.automation-phs-form-incomplete-error').getText()
    }

    // Income Modal Error messages
    getPhsIncomeModalRelationshipErrorMessage() {
        return $('.automation-phs-incomeModal-relationship-error').getText();
    }

    getPhsIncomeModalPurposeErrorMessage() {
        return $('.automation-phs-incomeModal-purpose-error').getText();
    }

    getPhsIncomeModalReasonableErrorMessage() {
        return $('.automation-phs-incomeModal-reasonable-error').getText();
    }

    getPhsIncomeModalStatementErrorMessage() {
        return $('.automation-phs-incomeModal-pub-statement-error').getText();
    }

    getPhsIncomeModalAllFieldsRequiredError() {
        return $('.automation-phs-incomeModal-all-required-error').getText();
    }

    // Income Modal non-publicly traded Error messages

    getPhsIncomeModalNPRelationshipListError() {
        return $('.automation-phs-IncomeModal-np-relationship-error').getText();
    }

    getPhsIncomeModalNPAllFieldsRequiredError() {
        return $('.automation-phs-incomeModal-np-items-missing-error').getText();
    }

    getPhsIncomeModalNPPurposeError() {
        return $('.automation-phs-incomeModal-np-purpose-error').getText();
    }

    getPhsIncomeModalNPReasonableError() {
        return $('.automation-phs-incomeModal-np-reasonable-error').getText();
    }

    getPhsIncomeModalNPStatementError() {
        return $('.automation-phs-incomeModal-np-statement-error').getText();
    }

    //Intellectual Property Modal errors
    getPhsIPModalEntityError() {
        return $('.automation-phs-ipModal-entity-error').getText();
    }

    getPhsIPModalAmountError() {
        return $('.automation-phs-ipModal-ip-amount-error').getText();
    }

    getPhsIPModalReplationshipError() {
        return $('.automation-phs-ipModal-ip-relationship-error').getText();
    }

    getPhsIpModalDescriptionError() {
        return $('.automation-phs-ipModal-ip-description-error').getText();
    }

    getPhsIpModalReasonableError() {
        return $('.automation-phs-ipModal-ip-reasonable-error').getText();
    }

    getPhsIpModalStatmenetError() {
        return $('.automation-phs-ipModal-ip-statement-error').getText();
    }

    // Travel modal error messages
    clickPhsTravelModalSubmitButton() {
        return $('.automation-phs-travelModal-submit-btn').click();
    }
    clickPhsTravelModalSubmitAgainButton() {
        return $('.automation-phs-travelModal-submit-again-btn').click();
    }

    getPhsTravelModalEntityError() {
        return $('.automation-travelModal-entity-error').getText();
    }

    getPhsTravelModalDestinationError() {
        return $('.automation-travelModal-destination-error').getText();
    }

    getPhsTravelModalTravelDateError() {
        return $$('.automation-travelModal-date-error').getText();
    }

    getPhsTravelModalTravelReasonError() {
        return $('.automation-travelModal-reason-error').getText();
    }

    getPhsTravelModalTravelReasonableError() {
        return $('.automation-travelModal-reasonable-error').getText();
    }

    getPhsTravelModalTravelStatementError() {
        return $('.automation-travelModal-statement-error').getText();
    }

    // management modal error messages

    clickPhsManagementModalSubmitButton() {
        return $('.automation-phs-manageModal-submit-btn').click();
    }

    getPhsManagementModalPositionError() {
        return $('.automation-manageModal-position-error').getText()
    }

    getPhsManagementModalResponsabilitiesError() {
        return $('.automation-manageModal-responsibility-error').getText()
    }

    // Publicly-traded income modal
    selectPhsIncomeModalRelationshipList(relationship) {
        // $('#npRelationship').click();
        return $('#relationship').element(by.cssContainingText('option', relationship)).click();
    }

    selectPhsIncomeModalNpRelationshipList(npRelationship) {
        // $('#npRelationship').click();
        return $('#npRelationship').element(by.cssContainingText('option', npRelationship)).click();
    }

    setPhsIncomeModalIncomeList(selectField, setAmount) {
        // return $(selectField)
        return $$('.automation-phs-incomeModal-table-label').then(function(options) {
            switch(selectField) {
                case 'Consulting':
                    $('.automation-phs-im-consulting').clear();
                    return $('.automation-phs-im-consulting').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Dividends':
                    $('.automation-phs-im-dividends').clear();
                    return $('.automation-phs-im-dividends').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Honoraria':
                    $('.automation-phs-im-honoraria').clear();
                    return $('.automation-phs-im-honoraria').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Authorship':
                    $('.automation-phs-im-authorship').clear();
                    return $('.automation-phs-im-authorship').sendKeys(setAmount + protractor.Key.RETURN);

                case 'In Kind':
                    $('.automation-phs-im-inKind').clear();
                    return $('.automation-phs-im-inKind').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Per Diem':
                    $('.automation-phs-im-diem').clear();
                    return $('.automation-phs-im-diem').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Salary':
                    $('.automation-phs-im-salary').clear();
                    return $('.automation-phs-im-salary').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Stipend':
                    $('.automation-phs-im-stipend').clear();
                    return $('.automation-phs-im-stipend').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Other':
                    $('.automation-phs-im-other-description').sendKeys('Protractor Tests' + protractor.Key.RETURN);
                    return $('.automation-phs-im-other-amount').sendKeys(setAmount + protractor.Key.RETURN)
            }
        });
    }

    setPhsIncomeModalIncomeNpList(selectField, setAmount) {
        // return $(selectField)
        return $$('.automation-phs-incomeModal-table-label').then(function(options) {
            switch(selectField) {
                case 'Consulting':
                    $('.automation-phs-im-np-consulting').clear();
                    return $('.automation-phs-im-np-consulting').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Dividends':
                    $('.automation-phs-im-np-dividends').clear();
                    return $('.automation-phs-im-np-dividends').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Honoraria':
                    $('.automation-phs-im-np-honoraria').clear();
                    return $('.automation-phs-im-np-honoraria').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Authorship':
                    $('.automation-phs-im-np-authorship').clear();
                    return $('.automation-phs-im-np-authorship').sendKeys(setAmount + protractor.Key.RETURN);

                case 'In Kind':
                    $('.automation-phs-im-np-inKind').clear();
                    return $('.automation-phs-im-np-inKind').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Per Diem':
                    $('.automation-phs-im-np-diem').clear();
                    return $('.automation-phs-im-np-diem').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Salary':
                    $('.automation-phs-im-np-salary').clear();
                    return $('.automation-phs-im-np-salary').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Stipend':
                    $('.automation-phs-im-np-stipend').clear();
                    return $('.automation-phs-im-np-stipend').sendKeys(setAmount + protractor.Key.RETURN);

                case 'Other':
                    $('.automation-phs-im-other-np-description').sendKeys('Protractor Tests' + protractor.Key.RETURN);
                    return $('.automation-phs-im-np-other-amount').sendKeys(setAmount + protractor.Key.RETURN)
            }
        });
    }

    getPhsIncomeModalAccumulatorTotal() {
        return $('.automation-phs-im-accumulator').getText();
    }

    getPhsIncomeModalNpAccumulatorTotal() {
        return $('.automation-phs-im-np-accumulator').getText();
    }

    isPhsIncomeModalNpAccumulatorTotalDisplayed() {
        return  $('.automation-phs-im-np-accumulator').isDisplayed()
    }

    setPhsIncomeModalBusinessPurposeStatement() {
        return $('.automation-phs-incomeModal-purpose-textbox')
            .sendKeys('Text set by Protractor Automated tests');
    }

    setPhsIncomeModalBusinessReasonableStatement() {
        return $('.automation-phs-incomeModal-reasonable-textbox')
            .sendKeys('Text set by Protractor Automated tests');
    }

    getPhsIncomeModalBusinessPurposeWordCount() {
        return $('.automation-phs-im-purpose-count').getText();
    }

    getPhsIncomeModalReasonableWordCount() {
        return $('.automation-phs-incomeModal-reasonable-count').getText();
    }

    setPhsIncomeModalNpBusinessPurposeStatement() {
        return $('.automation-phs-incomeModal-np-purpose-textbox')
            .sendKeys('Text set by Protractor Automated tests');
    }

    setPhsIncomeModalNpBusinessReasonableStatement() {
        return $('.automation-phs-incomeModal-np-reasonable-textbox')
            .sendKeys('Text set by Protractor Automated tests');
    }

    getPhsIncomeModaNplBusinessPurposeWordCount() {
        return $('.automation-phs-im-np-purpose-count').getText();
    }

    getPhsIncomeModalNpReasonableWordCount() {
        return $('.automation-phs-incomeModal-np-reasonable-count').getText();
    }

    selectPhsIpModalEntityList(ipEntity) {
        return $('#ipEntity.automation-phs-ipModal-entiy-select-list')
            .element(by.cssContainingText('option',ipEntity)).click();
    }

    selectPhsIpModalTotalAmountList(ipPayment) {
        return $('#ipPayment.automation-phs-ipModal-amount-select')
            .element(by.cssContainingText('option',ipPayment)).click();
    }

    selectPhsIpModalRelationshipList(ipRelationship) {
        return $('#ipRelationship.automation-phs-ipModal-relationship-select')
            .element(by.cssContainingText('option',ipRelationship)).click();
    }

    setPhsIpModalDescriptionOfIp() {
        return $('.automation-phs-ipModal-ip-description-textbox')
            .sendKeys('Text set by Protractor Automated tests');
    }

    setPhsIpModalIpStatement() {
        return $('.automation-phs-ipModal-statement-textbox')
            .sendKeys('Text set by Protractor Automated tests');
    }

    clickPhsIpModalCancelButton() {
        return $('.automation-phs-ipModal-cancel-btn').click();
    }

    selectPhsTravelModalEntityList(travelEntity) {
        return $('#travelEntityId.automation-phs-travelModal-entity-select')
            .element(by.cssContainingText('option',travelEntity)).click();
    }

    setPhsTravelModalDestinationTextbox() {
        return $('.automation-travelModal-destination-textbox')
            .sendKeys('Text set by Protractor Automated tests');
    }

    setPhsTravelModalReasonForTravelTextbox() {
        return $('.automation-phs-travelModal-travel-reason-textbox')
        .sendKeys('Text set by Protractor Automated tests');
    }

    setPhsTravelModalTravelStatementTextbox() {
        return $('.automation-phs-travelModal-travel-statement-textbox')
            .sendKeys('Text set by Protractor Automated tests');
    }

    clickPhsTravelModalCancelButton() {
        return $('.automation-phs-travelModal-cancel-btn').click();
    }

    selecttPhsMgmtModalEntityList(mgmEntity) {
        return $('#managementPosition.automation-phs-manageModal-position-select')
        .element(by.cssContainingText('option',mgmEntity)).click();
    }

    setPhsMgmtModalResponsabilityStatementTextbox() {
        return $('.automation-phs-manageModal-mgm-responsibilities-textbox')
            .sendKeys('Text set by Protractor Automated tests');
    }

    clickPhsMgmtModalCancelButton() {
        return $('.automation-phs-manageModal-cancel-btn').click();
    }

    clickPhsManagementModalSubmitAnotherButton() {
        return $('.automation-phs-manageModal-submitAnother-btn').click();
    }

    deletePhsIncomePublicSfiFiling() {
        return this.deletePhsSfiFiling('.automation-phs-income-sfi-public','public');
    }

    deletePhsIncomeNonPublicSfiFiling() {
        return this.deletePhsSfiFiling('.automation-phs-income-sfi-nonpublic-income','non-public');
    }

    deletePhsIncomeNonPublicEquitySfiFiling() {
        return this.deletePhsSfiFiling('.automation-phs-income-sfi-nonpublic-equity','non-public');
    }

    deletePhsIpSfiFiling() {
        return this.deletePhsSfiFiling('.automation-phs-ip-sfi','intellectual');
    }

    deletePhsTravelSfiFiling() {
        return this.deletePhsSfiFiling('.automation-phs-travel-sfi','travel');
    }

    deletePhsOutsideActivitySfiFiling() {
        return this.deletePhsSfiFiling('.automation-phs-management-sfi','management');
    }

    deletePhsSfiFiling(sfiToDelete, sfiType) {
        return $(sfiToDelete).isPresent().then(function(sfiFiling) {
            browser.sleep(750);
            $$(sfiToDelete).count().then(function(countSfi) {
                console.log('\n' + sfiType + ' Sfi count is: ' + countSfi);
                for(let i = 0; i < countSfi; i++) {
                    if(sfiFiling === true) {
                        browser.sleep(750);
                        $(sfiToDelete).click();
                        // console.log('\n clicked: ' + i);
                        browser.sleep(200);

                        switch(sfiType) {
                            case 'public':
                            $('.automation-phs-incomeModal-delete-btn').click();
                            $('.automation-phs-incomeModal-confirmDelete-btn').click();
                            browser.sleep(750);
                            break;

                            case 'non-public':
                            $('.automation-phs-incomeModal-np-delete-btn').click();
                            $('.automation-phs-incomeModal-np-delete-confirm-btn').click();
                            browser.sleep(750);
                            break;

                            case 'intellectual':
                            $('.automation-phs-ipModal-delete-btn').click();
                            $('.automation-phs-ipModal-delete-confirm-btn').click();
                            browser.sleep(750);
                            break;

                            case 'travel':
                            $('.automation-phs-travelModal-delete-btn').click();
                            $('.automation-phs-travelModal-deleteConfirm-btn').click();
                            browser.sleep(750);
                            break;

                            case 'management':
                            $('.automation-phs-manageModal-delete-btn').click();
                            $('.automation-phs-manageModal-delete-confirm-btn').click();
                            browser.sleep(750);
                            break;
                        }
                    }
                    else
                        return console.log('No Income Sfi\'s to delete');
                }
            });
        });
    }

    getSfiDataId() {
        return $$('.automation-phs-income-sfi-public').getAttribute('data-id');
    }

    /*
    startPhsFiling() {
        return $('.automation-phs-draft').isPresent().then(function(draft){
            if ( draft === true) {
                return $('.automation-phs-draft').click();
            }
            else
                $('.automation-phs-create-form').click();
        });
    }
    */
}

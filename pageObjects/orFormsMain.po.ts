import { browser, by, element, $, $$ } from 'protractor';

export class orFormsMain {

    navigateTo() {
        return browser.get(browser.baseUrl);
    }

    getorFormsTitle() {
        return $$( 'a.navbar-brand' ).getText();
    }

    getJumboTronText() {
        return $('.automation-welcome-msg').getText();
    }

    getJumboTronMsgText() {
        return $$('.automation-welcome-details').getText();
    }

    getCOIFormsMsgText() {
        return $('h2.coi-form-title').getText();
    }

    get700uFormTitleText() {
        return $('.automation-700u-caption').getText();
    }

    get700u30DayWarningText(){
        return $('.automation-f700-30-day').getText();
    }

    get700uFormStatementText() {
        return $('.automation-700u-h3').getText();
    }

    get800FormTitleText() {
        return $('.automation-800-caption').getText();
    }

    get800FormStatementText() {
        return $('.automation-800-h3').getText();
    }

    getPhsFormTitleText() {
        return $('.automation-phs-caption').getText();
    }

    getPhsFormStatementText() {
        return $('.automation-phs-h3').getText();
    }

    getSupplementFormTitleText() {
        return $('.automation-suppl-caption').getText();
    }

    getSupplementFormStatementText() {
        return $('.automation-suppl-h3').getText();
    }

    getNavbarText() {
        return $('.automation-navbar').getText();
    }

    getProjectMenuText() {
        return $('.automation-dropdown-button').getText();
    }

    clickOnProjectMenu() {
        return $('.automation-dropdown-button').click();
    }

    getPhsProjMenuItemText() {
        return $('.automation-phs-item').getText();
    }

    getNonPhsProjMenuItemText() {
        return $('.automation-nonphs-item').getText();
    }

    getMenuItemCount() {
        // return $$('.automation-dropdown-button').count();
        return $$('.automation-dropdown-menu li a').count();
    }

    // More generic detail data verification passing in the CSS selector and data to validate
    verifyDetails(elementToCheck, paramToCheck) {
        // return $$('app-award-detail tr.automation-subaward-row td').getText();
        let results = true;
        // console.log('awards= ' + browser.params.awards[1]);
        browser.sleep(1000);
        return $$(elementToCheck).getText().then(function(details) {
            for (let i = 0;  i < details.length; i++) {
                // console.log('from SIMS page:\n ' + details[i]);
                // console.log('from prop file:\n ' + paramToCheck[i]);
                if (details[i] !== paramToCheck[i]) {
                    console.log('String Comparison Failed!');
                    console.log('============ from UI page: =========\n ' + details[i]);
                    console.log('============ from properties: ======\n ' + paramToCheck[i]);
                    return results = false;
                }
            }
            return results = true;
            });
    }

    getLinksCount() {
        return $$('a').count();
    }

    getloggedInUserText() {
        return $('.automation-username').getText();
    }

    getFooterMessageText() {
        return $('.automation-footer').getText();
    }
}


import { browser, by, element, $, $$ } from 'protractor';

export class FormSuppPage {
    navigateTo() {
        return (browser.get(browser.baseUrl + 'landing?tab=formSupp'));
    }

    getFormSuppText() {
        return $('.automation-formSupp').getText();
    }

    getSuppTableHeaderText() {
        return $('.automation-supp-table-headers').getText();
    }

    getNoSuppFormSubmittedText() {
        return $ ('.automation-no-supplement').getText();
    }

}
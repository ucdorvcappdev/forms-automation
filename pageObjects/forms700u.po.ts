import { browser, by, element, $, $$, ExpectedConditions } from 'protractor';
import { protractor } from 'protractor/built/ptor';
import { Utils } from '../utils/utils.js';

export class Form700uPage {
    navigateTo() {
        // console.log(browser.get(browser.baseUrl + 'landing?tab=form700'));
        return (browser.get(browser.baseUrl + 'landing?tab=form700'));
    }

    getForm700Text() {
        return $('.automation-form700').getText();
    }

    getf700TableHeaders() {
        return $$('.automation-f700-table tr th').getText();
    }

    getf700CopyrightText() {
        return $$('div.automation-orforms-copyright').getText();
    }

    getf700FooterText() {
        return $$('div.automation-orform-footer').getText();
    }

    getNoF700MsgText() {
        return $$('.automation-no-f700').getText();
    }

    clickOnCreateForm700() {
        return $('.create700').click();
    }

    getcreatef700Url() {
        return (browser.getCurrentUrl());
    }

    getf700PageHeaderText() {
        return $('.form700Uheader').getText();
    }

    getf700LegendText() {
        return $('.automation-legend').getText();
    }

    getf700NameLabelText() {
        return $('.automation-f700-username-label').getText();
    }

    getf700EmailLabelText() {
        return $('.automation-f700-email-label').getText();
    }

    getf700DeptLabelText() {
        return $('.automation-f700-dept-label').getText();
    }
    getf700UsernameText() {
        return $('.automation-f700-username').getText();
    }

    getf700EmailText() {
        return $('.automation-f700-email').getText();
    }

    getf700DeptText() {
        return $('.automation-f700-dept').getText();
    }

    getf700FundingDetailsLabelText() {
        return $('.automation-legend-funding-label').getText();
    }

    getf700FundingWarningText() {
        return $('.automation-funding-warning').getText();
    }

    getf700ResearchLabelText() {
        return $('.automation-f700-research-label').getText();
    }
    getf700StatementTypeLabelText() {
        return $('.automation-f700-statement-type').getText();
    }
    getf700StatementTypesText() {
        return $$('.automation-statement-type-radio').getText();
    }

    getf700FundingDateLabelText() {
        return $$('.automation-f700-fund-date-label').getText();
    }

    getf700FundingDatePlaceholderText() {
        return $('.automation-f700-funddate-placeholder').getAttribute('placeholder');
    }

    getf700EntitySearchLabelText() {
        return $('.automation-f700entity-search-label').getText();
    }

    getf700EntitySearchPlaceHolderText() {
        return $$('.automation-f700-sponsor-textbox').getAttribute('placeholder');
    }

    getf700FundEntityResultLabelText() {
        return $('.automation-f700-fundentity-result-label').getText();
    }

    isf700FundEntityResultDisplayed(){
        return $('.automation-f700-fundentity-result').isDisplayed();
    }

    getf700FundAmountLabelText() {
        return $('.automation-f700-fundamount-label').getText();
    }

    getf700FundAmountContent() {
        return $('.automation-f700-fundamount-textbox').getText();
    }

    getf700EstimateLabelText(){
       return $('.automation-f700-estimate-label').getText();
    }

    getf700EstimateYesText(){
        return element.all(by.css('input#estimated1')).getAttribute('value');
    }

    getf700EstimateNoText(){
        return element.all(by.css('input#estimated2')).getAttribute('value');
    }

    getf700HumanSubjectLabelText(){
        return $$('.automation-f700-human-subject-label').getText();
    }

    getf700HumanSubjectYesOption() {
        return $$('input#humanSubjects1').getAttribute('value');
    }
    getf700HumanSubjectNoOption() {
        return $$('input#humanSubjects2').getAttribute('value');
    }

    getf700FilerLegendLabelText() {
        return $('.automation-legend-filer-label').getText();
    }

    getf700AssociationLabelText() {
        return $$('.automation-f700-association-label').getText();
    }

    getf700AssociationTitleLabelText() {
        return $('.automation-f700-title-label').getText();
    }

    getf700AssociationTitlePlaceholderText() {
        return $('.automation-f700-title-textbox').getAttribute('placeholder');
    }

    getf700InvestmentTitleLabelText() {
        return $$('.automation-f700-investment-title').getText();
    }

    isf700InvestmentYesOptionDisplayed() {
        return $('#thresholdInvestment1').isDisplayed();
    }

    isf700InvestmentNoOptionDisplayed() {
        return $('#thresholdInvestment2').isDisplayed();
    }

    getf700IncomeTitleLabelText() {
        return $$('.automation-f700-income-title').getText();
    }

    isf700IncomeYesOptionDisplayed() {
        return $('#thresholdIncome1').isDisplayed();
    }

    isf700IncomeNoOptionDisplayed() {
        return $('#thresholdIncome2').isDisplayed();
    }

    getf700LoanTitleLabelText() {
        return $$('.automation-f700-loan-label').getText();
    }

    isf700LoanIncomeYesOptionDisplayed() {
        return $('#thresholdLoan1').isDisplayed();
    }

    isf700LoanIncomeNoOptionDisplayed() {
        return $('#thresholdLoan2').isDisplayed();
    }
    getf700GiftsTitleLabelText() {
        return $$('.automation-f700-gifts-label').getText();
    }

    isf700GiftsYesOptionDisplayed() {
        return $('#thresholdGift1').isDisplayed();
    }

    isf700GiftsNoOptionDisplayed() {
        return $('#thresholdGift2').isDisplayed();
    }

    getf700TravelTitleLabelText() {
        return $$('.automation-f700-travel-label').getText();
    }

    isf700TravelYesOptionDisplayed() {
        return $('#paidTravel1').isDisplayed();
    }

    isf700TravelNoOptionDisplayed() {
        return $('#paidTravel2').isDisplayed();
    }

    getf700CancelButtonText() {
        return $('.automation-f700-user-cancel-btn').getText();
    }

    getF700SaveDraftButtonText() {
        return $('.automation-f700-user-savedraft-btn').getText();
    }

    getf700AssocHelpText() {
        return $('.automation-f700-assoc-help').getText();
    }

    getf700InvestHelpText() {
        return $('.automation-f700-investment-help').getText();
    }

    getf700IncomeHelpText() {
        return $('.automation-f700-income-help').getText();
    }

    getf700LoanHelpText() {
        return $('.automation-f700-loan-help').getText();
    }

    getf700GiftsHelpText() {
        return $('.automation-f700-gifts-help').getText();
    }

    getf700TravelHelpText() {
        return $('.automation-f700-travel-help').getText();
    }

    getf700VerificationHelpText() {
        return $('.automation-f700-verification-help').getText();
    }

    getNoProjectLinkLabelText() {
        return $('.automation-f700-noprject-link-label').getText();
    }
    isNoProjectLinkDisplayed() {
        return $('.automation-f700-noprject-link-label').isDisplayed();
    }

    f700selectProject() {
        $('#researchProject').click();
        return element(by.cssContainingText('option','OR Forms automation project')).click();
    }

    f700setInitialNewFundingOption() {
        return $('#statementType1').click();
    }

    f700setInterimFundingOption() {
        return $('#statementType2').click();
    }

    f700setFundingDate() {
        $('.automation-f700-funddate-placeholder').sendKeys(this.randomDate());
    }

    f700setSponsor(sponsorName) {
        // const EC=protractor.ExpectedConditions;
        $$('input.automation-f700-sponsor-textbox').sendKeys(sponsorName);
        browser.sleep(2000);
        return $('input.automation-f700-sponsor-textbox').getText();
    }

    getf700SponsorListText() {
        return $$('.automation-f700-sponsor-result').getText();
    }

    selectf700Sponsor(sponsor) {
        $$('.automation-f700-sponsor-result').filter(function(elem,index) {
            return elem.getText().then(function(text) {
                return text === sponsor;
            });
        }).first().click();
        browser.sleep(1000);
    }

    getf700FundEntityResultText(){
        return $('.automation-f700-fundentity-result').getText();
    }

    setf700FundAmount(amount) {
        return $('.automation-f700-fundamount-textbox').sendKeys(amount);
    }

    getf700FundAmountValue() {
        return $('.automation-f700-fundamount-textbox').getAttribute('value');
    }

    selectf700YesNoOption(myCss, option) {
        return $$('.radio').then(function(options) {
            if (option === 'YES') {
                return $(myCss + '1').click();
            } else {
                return $(myCss + '2').click();
            }
        });
    }

    setf700Signature() {
        // var u = new Utils;
        return $$('.signature').sendKeys('Signed by automated tests on: ' + this.todaysDate());
    }

    clickOnf700CancelButton() {
        return $('.automation-f700-user-cancel-btn').click();
    }

    clickF700SaveDraftButton() {
        return $('.automation-f700-user-savedraft-btn').click();
    }

    getf700ErrorDocNotSavedText() {
        return $('.automation-doc-not-saved').getText();
    }

    getf700ErrorProjectText() {
        return $('.automation-f700-project-error').getText();
    }

    getf700ErrorStatement() {
        return $('.automation-f700-statement-error').getText();
    }

    getf700ErrorEntity() {
        return $('.automation-f700-entity-error').getText();
    }

    getf700ErrorFundAmount() {
        return $('.automation-f700-amount-error').getText();
    }

    getf700ErrorEstimateQuestion() {
        return $('.automation-f700-estimate-error').getText();
    }

    getf700ErrorHumanSubject() {
        return $('.automation-f700-hs-error').getText();
    }

    getf700ErrorAssociation() {
        return $('.automation-f700-assoc-error').getText();
    }

    getf700ErrorInvestment() {
        return $('.automation-f700-invest-error').getText();
    }

    getf700ErrorIncome() {
        return $('.automation-f700-income-error').getText();
    }

    getf700ErrorSpouseIncome() {
        return $('.automation-f700-spouse-income-error').getText();
    }

    getf700ErrorLoan() {
        return $('.automation-f700-loan-error').getText();
    }

    getf700ErrorLoanAmount() {
        return $('.automation-f700-loan-amt-error').getText();
    }

    getf700ErrorLoanType() {
        return $('.automation-f700-loan-type-error').getText();
    }

    getf700ErrorLoanRate() {
        return $('.automation-f700-loan-rate-error').getText();
    }

    getf700ErrorLoanRepaid() {
        return $('.automation-f700-loan-repaid-error').getText();
    }

    getf700ErrorGift() {
        return $('.automation-f700-gift-error').getText();
    }

    getf700ErrorGiftValue() {
        return $('.automation-f700-gift-value-error').getText();
    }

    getf700ErrorGiftDate() {
        return $('.automation-f700-gift-date-error').getText();
    }

    getf700ErrorTravelAmount() {
        return $('.automation-f700-travel-amnt-error').getText();
    }

    getf700ErrorTravelPaymentAmount() {
        return $('.automation-f700-travel-pymt-error').getText();
    }

    getf700ErrorTravelDescription() {
        return $('.automation-f700-travel-descrp-error').getText();
    }

    getf700ErrorTravelStartDate() {
        return $('.automation-f700-travel-start-error').getText();
    }

    getf700ErrorTravelEndDate() {
        return $('.automation-f700-travel-end-error').getText();
    }

    clickFinalizeButton() {
        return $('.automation-f700-user-finalize-btn').click();
    }

    setf700AssociationText(text2Set) {
        return $('.automation-f700-title-textbox').sendKeys(text2Set);
    }

    getf700AssociationValueText() {
        return $('.automation-f700-title-textbox').getAttribute('value');
    }

    getf700SupplementalFormRequiredMsgText(){
        return $('.automation-f700-suppl-required').getText();
    }

    clickOnOption(cssOption) {
        return $(cssOption).click();
    }

    setf700InterestRate(loanRate) {
        return $('.automation-loan-rate').sendKeys(loanRate);
    }

    setf700GiftDescription() {
        return $('.automation-f700-gift-description').sendKeys('Test description - automated protractor tests');
    }

    setf700GiftValue(setValue) {
        return $('.automation-f700-gift-value').sendKeys(setValue);
    }

    setf700GiftDateReceived(setDate) {
        return $('.automation-f700-gift-date').sendKeys(setDate)
    }

    setf700TravelPaidAmount(amountPaid) {
        return $('.automation-f700-travel-amount').sendKeys(amountPaid);
    }

    setf700TravelDescription(travelDescription) {
        return $('.automation-f700-travel-input').sendKeys(travelDescription);
    }

    setf700TravelStartDate(travelStartDate) {
        return $('.automation-f700-travel-start-date').sendKeys(travelStartDate);
    }

    setf700TravelEndDate(travelEndDate) {
        return $('.automation-f700-travel-end-date').sendKeys(travelEndDate);
    }

    getf700InvestmentValueLabel() {
        return $('.automation-f700-investment-value-label').getText();
    }

    getf700InvestmentDateDisposedLabel() {
        return $('.automation-f700-invest-date-disposed').getText();
    }

    randomDate() {
        var currentTime = new Date();
        var myYear = currentTime.getFullYear() + 1;
        var sec = currentTime.getSeconds();
        var month = currentTime.getMonth();
        var day = currentTime.getDate();
        var futureDate = ( 1 + Math.floor((sec/5)%9) + "/" + (day + (sec%9)) + "/" +( myYear + (sec%9)));
        // console.log('month: '+month);
        // console.log('seconds: '+sec);
        // console.log('month mod: '+  Math.floor((sec/5)%9));
        return futureDate;
    }
    todaysDate() {
        var currentTime = new Date();
        var myYear = currentTime.getFullYear();
        var sec = currentTime.getSeconds();
        var month = currentTime.getMonth()+1;
        var day = currentTime.getDate();
        var todayDate = ( month + "/" + day + "/" + myYear);
        // console.log('month: '+month);
        // console.log('seconds: '+sec);
        // console.log('month mod: '+  Math.floor((sec/5)%9));
        // var todayDate = ( 1 + Math.floor((sec/5)%9) + "/" + (day + (sec%9)) + "/" +( myYear + (sec%9));
        return todayDate;
    }
}
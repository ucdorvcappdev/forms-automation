import { browser, by, element, $, $$ } from 'protractor';

export class Form800Page {
    navigateTo() {
        return (browser.get(browser.baseUrl + 'landing?tab=form800'));
    }

    getForm800Text() {
        return $('.automation-form800').getText();
    }

    getf800TableHeader() {
        return $$('.automation-f800-table-headers th').getText();
    }

    getf800NoSubmittedFormText() {
        return $('.automation-no-f800').getText();
    }

    clickOnCreate800FormLink() {
        return $('.automation-create800').click();
    }

    getBuildForm800URL() {
        return (browser.getCurrentUrl());
    }

    getF800HeaderText() {
        return $('.form800header').getText();
    }

    getF800Ppm230Text() {
        return $('.automation-f800-ppm230-text').getText();
    }

    getF800SfiText() {
        return $('.automation-f800-sfi-text').getText();
    }

    getF800LegendLabel() {
        return $('legend').getText();
    }

    getF800DemographicsLabel() {
        return $('.automation-f800-demographics').getText();
    }

    getF800PiNameLabel() {
        return $('.automation-f800-name-label').getText();
    }

    getF800PiNameFieldValue() {
        return $('.automation-f800-name-value').getText();
    }

    getF800PiEmailLabel() {
        return $('.automation-f800-email-label').getText();
    }

    getF800PiEmailFieldValue() {
        return $('.automation-f800-email-value').getText();
    }

    getF800PiDeptLabel() {
        return $('.automation-f800-dept-label').getText();
    }

    getF800PiDeptFieldValue() {
        return $('.automation-f800-dept-value').getText();
    }

    getFrom800Label() {
        return $('.automation-form800-section-label').getText();
    }

    getF800ProjDropDownLabel() {
        return $('.automation-f800-research-project-label').getText();
    }

    getF800ProposalTypeLabel() {
        return $('.automation-f800-disclosure-type-label').getText();
    }

    getF800ProjDropDownValue() {
        return $('.automation-f800-research-project-label').getText();
    }

    getF800ProposalTypeSelection() {
        return $('.automation-f800-disclosure-type-label').getText();
    }

    getF800ProposalTypeText() {
        return $$('.automation-f800-radio-proposal label').getText();

    }

    getF800PreviousAwardLabel() {
        return $('.automation-f800-prev-award-label').getText();
    }

    getF800PreviousAwardPlaceholderText() {
        return $('.automation-f800-prev-award-textbox').getAttribute('placeholder');
    }

    setF800PrevAwardNumber(awardNumber) {
        return $('.automation-f800-prev-award-textbox').sendKeys(awardNumber);
    }

    getF800PreviousAwardTextbox() {
        return $('.automation-f800-prev-award-textbox').getText();
    }

    getF800FundingEntitySearchLabel() {
        return $('.automation-f800-entity-search-label').getText();
    }

    getF800FundingEntityPlaceholderText() {
        // return $('#search').getAttribute('placeholder');
        return $$('input.automation-f800-fund-entity-textbox').getAttribute('placeholder');
    }

    getF800FundingEntitySearchTextbox() {
        return $('.automation-f800-fund-entity-textbox').getText();
    }

    getF800FundingSearchResultLabel(){
        return $('.automation-f800-entity-result-label').getText();
    }

    getF800FundingTypeLabel(){
        return $('.automation-f800-entity-type-label').getText();
    }

    getF800BudgetFromLabel(){
        return $('.automation-f800-budget-start-date-label').getText();
    }

    getF800BudgetToLabel(){
        return $('.automation-f800-budget-end-date-label').getText();
    }

    getF800ProjectBeginDateLabel(){
        return $('.automation-f800-proj-start-date-label').getText();
    }

    getF800ProjectEndDateLabel(){
        return $('.automation-f800-proj-end-date-label').getText();
    }

    getF800FundingAmountLabel(){
        return $('.automation-f800-funding-label').getText();
    }

    getF800FundingAmountPlaceholderText(){
        return $('input.automation-f800-funding-textbox').getAttribute('placeholder');
    }

    getF800FundingAmountTextbox(){
        return $('input.automation-f800-funding-textbox').getText();
    }

    getF800EstimateQuestionLabel(){
        return $('.automation-f800-estimate-question').getText();
    }

    getF800HumanSubectIrbLabel(){
        return $('.automation-f800-IRB-label').getText();
    }

    getF800HumanSubjectQuestionLabel(){
        return $('.automation-f800-IRB-question').getText();
    }

    getF800IrbNetIdLabel(){
        return $('.automation-f800-irbnetid-label').getText();
    }

    getF800IrbPlaceholderText(){
        return $('.automation-f800-irb-textbox').getAttribute('placeholder');
    }

    getF800IrbInputText() {
        return $('.automation-f800-irb-textbox').getAttribute('value');
    }

    f800EnterIRBNetId(digits) {
        // return $('.automation-f800-irb-textbox').sendKeys('1234567');
        return $('.automation-f800-irb-textbox').sendKeys(this.getRandom(digits));
    }

    setf800IRBNetId(irbNetId) {
        return $('.automation-f800-irb-textbox').sendKeys(irbNetId);
    }

    getF800SfiDisclosureLabel(){
        return $('.automation-f800-sfidisclosure-label').getText();
    }

    getF800SfiDisclosureText(){
        return $('.automation-f800-sfidisclosure-text').getText();
    }

    getF800SignSubmitLabel(){
        return $('.automation-f800-sign-submit-label').getText();
    }

    getF800VerificationLabel(){
        return $('.automation-f800-verification-pi-label').getText();
    }

    getF800VerificationText(){
        return $('.automation-f800-verification-text').getText();
    }

    getF800SignatureLabel(){
        return $('.automation-f800-signature-label').getText();
    }

    getF800YesNoOptionText(cssoption) {
        return $$('.automation-f800-' + cssoption +'-radio label').getText();
    }

    getF800CancelButtonExist() {
        return $('.automation-f800-cancel-button').isPresent();
    }

    getF800SaveDraftButtonText() {
        return $('.automation-f800-save-draft-button').isPresent();
    }

    getF800FinalizeButtonText() {
        return $('.automation-f800-finalize-button').getText();
    }

    isF800FinalizeButtonEnabled() {
        return $('.automation-f800-finalize-button').isEnabled();
    }

    f800ClickSaveDraftButton() {
        return $('.automation-f800-save-draft-button').click();
    }

    f800DocumentErrorIsDisplayed() {
        return $('.automation-f800-document-error-text').isDisplayed();
    }

    getF800DocumentErrorText() {
        return $('.automation-f800-document-error-text').getText();
    }

    f800DocumentNotSavedErrorIsDisplayed() {
        return $('.automation-f800-doc-not-saved-text').isDisplayed();
    }

    getF800DocumentNotSavedErrorText() {
        return $('.automation-f800-doc-not-saved-text').getText();
    }

    f800MissingResearchProjErrorIsDisplayed() {
        return $('.automation-f800-missing-project-error').isDisplayed();
    }

    getF800MissingResearchProjErrorText() {
        return $('.automation-f800-missing-project-error').getText();
    }

    f800MissingEntityErrorIsDisplayed() {
        return $('.automation-f800-search-error').isDisplayed();
    }

    getF800MissingEntityErrorText() {
        return $('.automation-f800-search-error').getText();
    }

    f800MissingFundingErrorIsDisplayed() {
        return $('.automation-f800-missing-funding-error').isDisplayed();
    }

    getF800MissingFundingErrorText() {
        return $('.automation-f800-missing-funding-error').getText();
    }

    f800MissingBudgetStartErrorIsDisplayed() {
        return $('.automation-f800-budget-startdate-error').isDisplayed();
    }

    f800MissingBudgetEndErrorIsDisplayed() {
        return $('.automation-f800-budget-enddate-error').isDisplayed();
    }

    f800MissingProjStartDateErrorIsDisplayed() {
        return $('.automation-f800-projbegin-date-error').isDisplayed();
    }

    f800MissingProjEndDateErrorIsDisplayed() {
        return $('.automation-f800-projend-date-error').isDisplayed();
    }

    f800MissingFundingAmountErrorIsDisplayed() {
        return $('.automation-f800-funding-error').isDisplayed();
    }

    f800MissingEstimateQuestionErrorIsDisplayed() {
        return $('.automation-f800-estimate-error').isDisplayed();
    }

    f800MissingHumanSubjectQuestionErrorIsDisplayed() {
        return $('.automation-f800-human-subject-error').isDisplayed();
    }

    f800MissingIrbNetErrorIsDisplayed() {
        return $('.automation-f800-missing-irb-error').isDisplayed();
    }

    getF800MissingIrbNetErrorText() {
        return $('.automation-f800-missing-irb-error').getText();
    }

    f800SupplementalRequiredMessageIsDisplayed() {
        return $('.automation-f800-suppl-required-message').isDisplayed();
    }

    getf800SupplementRequiredMessageText() {
        return $('.automation-f800-suppl-required-message').getText();
    }

    f800EnterSignature(mySignature) {
        return $('.automation-f800-signature-textbox').sendKeys(mySignature + this.todaysDate());
    }

    getf800SignatureText() {
        return $('.automation-f800-signature-textbox').getAttribute('value');
    }

    f800ClickFinalizeButton() {
        return $('.automation-f800-finalize-button').click();
    }

    selectF800YesNoOption(myCss, option) {
        return $$('.radio').then(function(options) {
            if (option === 'YES') {
                return $(myCss + '1').click();
            } else
            return $(myCss + '2').click();
        });
    }

    getf800AdditionalFundingLabel() {
        return $('.automation-f800-add-fund-label').getText();
    }

    setF800AdditionFundingAmount(amount) {
        return $('.automation-f800-add-fund-input').sendKeys(amount);
    }

    getF800PriorSponsorLabel() {
        return $('.automation-f800-prior-sponsor-label').getText();
    }

    setF800PriorSponsorInput(priorSponsor) {
        return $('.automation-f800-prior-sponsor-input').sendKeys(priorSponsor);
    }

    getF800PriorPINameLabel() {
        return $('.automation-f800-priorname-label').getText();
    }

    setF800PriorPINameInput(piName) {
        return $('.automation-f800-priorname-input').sendKeys(piName);
    }

    selectProposalType(proposalType) {
        return $$('.radio').then(function(options) {
            switch (proposalType) {
                case 'New':
                    return $$('#statementType1').click();
                case 'Continuation':
                    return $$('#statementType2').click();
                case 'New Sponsor':
                    return $$('#statementType3').click();
                case 'Add Investigator':
                    return $$('#statementType4').click();
                case 'Change PI':
                    return $$('#statementType5').click();
                case 'New SFI':
                    return $$('#statementType6').click();
            }
        });
    }

    f800selectProject() {
        $('#researchProject').click();
        return element(by.cssContainingText('option','OR Forms automation project')).click();
    }

    f800setFundingEntity(entity) {
        return $$('.automation-f800-fund-entity-textbox').sendKeys(entity);
    }

    getf800EntityListText() {
        return $('.automation-f800-entity-result').getText();
    }

    getf800SelectedEntityText() {
        return $('.automation-f800-selected-entity').getText();
    }

    selectf800Entity(sponsor) {
        $$('.automation-f800-entity-result').filter(function(elem,index) {
            return elem.getText().then(function(text) {
                return text === sponsor;
            });
        }).first().click();
        browser.sleep(1000);
    }

    selectF800SponsorType(sponsorType) {
        $('.automation-f800-sponsor-list').click();
        return element(by.cssContainingText('option',sponsorType)).click();
    }

    setf800FundingAmount(fundingAmount) {
        return $('input.automation-f800-funding-textbox').sendKeys(fundingAmount);
    }

    getf800FundingAmountValue() {
        return $('.automation-f800-funding-textbox').getText();
    }

    setf800BudgetDateFrom() {
        return $('.automation-f800-budget-from-input').sendKeys(this.todaysDate());
    }

    setf800BudgetDateTo() {
        return $('.automation-f800-budget-to-input').sendKeys(this.randomDate());
    }

    setf800ProjectDateFrom() {
        return $('.automation-f800-project-startdate-input').sendKeys(this.todaysDate());
    }

    setf800ProjectDateTo() {
        return $('.automation-f800-project-enddate-input').sendKeys(this.randomDate());
    }

    randomDate() {
        var currentTime = new Date();
        var myYear = currentTime.getFullYear() + 1;
        var sec = currentTime.getSeconds();
        var month = currentTime.getMonth();
        var day = currentTime.getDate();
        var futureDate = ( 1 + Math.floor((sec/5)%9) + "/" + (day + (sec%9)) + "/" +( myYear + (sec%9)));
        // console.log('month: '+month);
        // console.log('seconds: '+sec);
        // console.log('month mod: '+  Math.floor((sec/5)%9));
        return futureDate;
    }
    todaysDate() {
        var currentTime = new Date();
        var myYear = currentTime.getFullYear();
        var sec = currentTime.getSeconds();
        var month = currentTime.getMonth()+1;
        var day = currentTime.getDate();
        var todayDate = ( month + "/" + day + "/" + myYear);
        // console.log('month: '+month);
        // console.log('seconds: '+sec);
        // console.log('month mod: '+  Math.floor((sec/5)%9));
        // var todayDate = ( 1 + Math.floor((sec/5)%9) + "/" + (day + (sec%9)) + "/" +( myYear + (sec%9));
        return todayDate;
    }

    getRandom(length) {
        return Math.floor(Math.pow(10, length-1) + Math.random() * 9 * Math.pow(10, length-1));
        }
}



        // switch (proposalType) {
            // case 'New':
            //     // return $('li.NEW_PROJECT label').getText();
            //     return element(by.tagName('.automation-f800-radio-proposal label')).getText();
            //     // return $('.NEW_PROJECT').then (function() {
            //     //     return element(by.tagName('label for')).getText();
            //     // });
            //     break;
            // case 'Continuation':
            //     return $$('.automation-f800-radio-proposal label').getText();
            //     // return element(by.tagName('.automation-f800-radio-proposal label')).getText();
            //     break;
            //     // document.querySelector("body > div.container-fluid > div.col-md-8.well.bs-component > form > fieldset > div:nth-child(4) > div:nth-child(3) > div.col-md-10 > div > ul > li:nth-child(1) > label")        
            // }


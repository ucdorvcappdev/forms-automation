import { orFormsMain } from '../pageObjects/orFormsMain.po';
import { browser } from 'protractor';
// import { TestObject } from 'protractor/built/driverProviders';

describe('OR Forms Main index page UI verification tests', () => {
    let mainForms: orFormsMain;

    beforeEach(() => {
       mainForms = new orFormsMain();
    });

    it('should login and open main landing page of COI Forms: test #01', function () {
        mainForms.navigateTo();
        expect(mainForms.getorFormsTitle()).toMatch('University of California Davis - Office of Research');
    });

    it('should return the welcome title: test #02', function () {
        mainForms.navigateTo();
        expect(mainForms.getJumboTronText()).toContain('Electronic Conflict of Interest (eCOI)');
    });

    it('should return the welcome message title: test #03', function () {
        mainForms.navigateTo();
        expect(mainForms.getJumboTronMsgText()).toMatch('Online Form Submission');
    });

    // ***** Code block REMOVED FROM CODE commit #2e25385193d51
    // it('should return the COI forms message title: test #04', function () {
    //     mainForms.navigateTo();
    //     expect(mainForms.getCOIFormsMsgText()).toContain('Conflict of Interest (COI) Reporting Online Forms');
    // });

    it('should return 700-U 30 day warning: test #04', function () {
        mainForms.navigateTo();
        expect(mainForms.get700u30DayWarningText()).toContain('Must be filed within 30 days of receipt of new funds.');
    });

    // ***** Code block REMOVED FROM CODE commit #2e25385193d51
    // it('should return form 700u title: test #05', function () {
    //     mainForms.navigateTo();
    //     expect(mainForms.get700uFormTitleText()).toContain('Form 700-U');
    // });

    it('should return form 700u statement: test #06', function () {
        mainForms.navigateTo();
        expect(mainForms.get700uFormStatementText()).toContain('700-U (Private Sponsors)');
    });

    it('should return form 700u statement details: test #07', function () {
        mainForms.navigateTo();
        expect(mainForms.verifyDetails('.automation-700-details',browser.params.SevenHundredInfo)).toBe(true);
    });

    // ***** Code block REMOVED FROM CODE commit #2e25385193d51
    // it('should return form 800 title: test #08', function () {
    //     mainForms.navigateTo();
    //     expect(mainForms.get800FormTitleText()).toContain('Form 800');
    // });

    it('should return form 800 statement: test #09', function () {
        mainForms.navigateTo();
        expect(mainForms.get800FormStatementText()).toContain('Form 800 (Non-PHS Government Sponsors/Human Subject Research)');
    });

    it('should return form 800 statement details: test #10', function () {
        mainForms.navigateTo();
        expect(mainForms.verifyDetails('.automation-800-details',browser.params.EightHunderedInfo)).toBe(true);
    });

    // ***** Code block REMOVED FROM CODE commit #2e25385193d51
    // it('should return form PHS title: test #11', function () {
    //     mainForms.navigateTo();
    //     expect(mainForms.getPhsFormTitleText()).toContain('Form PHS (Public Health Service)');
    // });

    it('should return form PHS statement: test #12', function () {
        mainForms.navigateTo();
        expect(mainForms.getPhsFormStatementText()).toContain('PHS (Public Health Service Sponsors)');
    });

    it('should return form PHS statement details: test #13', function () {
        mainForms.navigateTo();
        expect(mainForms.verifyDetails('.automation-phs-details',browser.params.PhsInfo)).toBe(true);
    });

    // ***** Code block REMOVED FROM CODE commit #2e25385193d51
    // it('should return Supplemental Form title: test #14', function () {
    //     mainForms.navigateTo();
    //     expect(mainForms.getSupplementFormTitleText()).toContain('Statement of Economic Interests - Supplemental Form');
    // });

    it('should return Supplemental Form statement: test #15', function () {
        mainForms.navigateTo();
        expect(mainForms.getSupplementFormStatementText()).toContain('Supplemental Form');
    });

    it('should return Supplemental form statement details: test #16', function () {
        mainForms.navigateTo();
        expect(mainForms.verifyDetails('.automation-suppl-details',browser.params.SupplementInfo)).toBe(true);
    });

    it('should check the navbar text successfully: test #17', function() {
        mainForms.navigateTo();
        expect(mainForms.getNavbarText()).toMatch('University of California Davis - Office of Research');
    });

    it('should check the text of the menu item and verify that there are 2 submenus: test #18', function() {
        mainForms.navigateTo();
        expect(mainForms.getProjectMenuText()).toMatch('Project Information');
        mainForms.clickOnProjectMenu();
        expect(mainForms.getMenuItemCount()).toBe(2);
        // browser.sleep(1000);
        expect(mainForms.getPhsProjMenuItemText()).toMatch('PHS Projects');
        expect(mainForms.getNonPhsProjMenuItemText()).toMatch('Non-PHS Projects');
    });

    it('should verify that the number of links equal to 9: test #19', function() {
        mainForms.navigateTo();
        expect(mainForms.getLinksCount()).toBe(9);
    });

    it('should verify that the username returns correctly : test #20', function() {
        mainForms.navigateTo();
        expect(mainForms.getloggedInUserText()).toMatch('ortester');
    });

    it('should return the correct footer message : test #21', function() {
        mainForms.navigateTo();
        expect(mainForms.getFooterMessageText()).toContain('Browser Compatability Note: Must use Chrome, Firefox, Internet Explorer (current version Edge) or the most current version of your chosen browser.');
    });
});

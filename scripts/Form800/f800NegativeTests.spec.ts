import { Form800Page } from '../../pageObjects/forms800.po';
import { browser } from 'protractor';

describe('Form 800 Tests', () => {

    let form800page:  Form800Page;

    beforeEach(() => {
        form800page = new Form800Page();
     });

    it('should verify basic error messages when user hits Save Draft w/o entering data - Negative test #01', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        form800page.f800ClickSaveDraftButton();
        expect(form800page.f800DocumentErrorIsDisplayed()).toBe(true);
        expect(form800page.getF800DocumentErrorText()).toContain('! - Document not saved. Errors exist on page.');
        expect(form800page.f800DocumentNotSavedErrorIsDisplayed()).toBe(true);
        expect(form800page.getF800DocumentNotSavedErrorText()).toContain('Document not saved. Errors exist on page.')
        expect(form800page.f800MissingResearchProjErrorIsDisplayed()).toBe(true);
        expect(form800page.getF800MissingResearchProjErrorText()).toContain('A research project is required');
        expect(form800page.f800MissingEntityErrorIsDisplayed()).toBe(true);
        expect(form800page.getF800MissingEntityErrorText()).toContain('Select a funding entity');
        browser.sleep(555);
    });


    it('should verify Error messages when user Finalizing form w/o entering other data - Negative test #02', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        form800page.f800EnterSignature('Signed by Protractor Automation');
        expect(form800page.getf800SignatureText()).toContain('Signed by Protractor Automation');
        form800page.f800ClickFinalizeButton();
        expect(form800page.f800MissingFundingErrorIsDisplayed()).toBe(true);
        expect(form800page.getF800MissingFundingErrorText()).toContain('A funding entity type is required')
        expect(form800page.f800MissingBudgetStartErrorIsDisplayed()).toBe(true);
        expect(form800page.f800MissingBudgetEndErrorIsDisplayed()).toBe(true);
        expect(form800page.f800MissingProjStartDateErrorIsDisplayed()).toBe(true);
        expect(form800page.f800MissingProjEndDateErrorIsDisplayed()).toBe(true);
        expect(form800page.f800MissingFundingErrorIsDisplayed()).toBe(true);
        expect(form800page.f800MissingEstimateQuestionErrorIsDisplayed()).toBe(true);
        expect(form800page.f800MissingHumanSubjectQuestionErrorIsDisplayed()).toBe(true);
        browser.sleep(500);
    });


    it('should verify that the IRBNet ID field error message is displayed - Negative test #03', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        form800page.selectF800YesNoOption('#humanSubjects', 'YES');
        form800page.f800EnterSignature('Signed by Protractor Automation');
        expect(form800page.getf800SignatureText()).toContain('Signed by Protractor Automation');
        form800page.f800ClickFinalizeButton();
        expect(form800page.f800MissingIrbNetErrorIsDisplayed()).toBe(true);
        expect(form800page.getF800MissingIrbNetErrorText()).toContain('Please enter valid IRBNet # (6 or 7 digit number)');
    });

    // it('should verify Form 800 Demographics section headings - test #07', function() {
    //     form800page.navigateTo();
    //     form800page.clickOnCreate800FormLink();
    //     expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
    // });

});
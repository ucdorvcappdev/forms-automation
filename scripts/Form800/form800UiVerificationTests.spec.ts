import { Form800Page } from '../../pageObjects/forms800.po';
import { browser } from 'protractor';

describe('Form 800 Tests', () => {

    let form800page:  Form800Page;

    beforeEach(() => {
        form800page = new Form800Page();
     });

    it('should display form 800 tab - test #01', function() {
        form800page.navigateTo();
        expect(form800page.getForm800Text()).toContain('Statement of Economic Interest');
    });

    it('should verify form 800 table headers - test #02', function() {
        form800page.navigateTo();
        expect(form800page.getf800TableHeader()).toEqual(browser.params.Form800TableHeaders);
    });

    it('should verify no submitted form 800 message - test #03', function() {
        form800page.navigateTo();
        expect(form800page.getf800NoSubmittedFormText()).toContain('You have no Form 800\'s submitted.');
    });

    it('should verify that clicking on Create New opens an empty Form 800 and verifies existence of the header - test #04', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        expect(form800page.getF800HeaderText()).toContain('Form 800 - Statement of Economic Interest');
        // expect(form800page.getf800NoSubmittedFormText()).toContain('You have no Form 800\'s submitted.');
    });

    it('should verify PPM 230-05 and SFI paragraph contents - test #05', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        expect(form800page.getF800Ppm230Text()).toContain(browser.params.Form800PPM230Message);
        expect(form800page.getF800SfiText()).toContain(browser.params.Form800SFIMessage);
    });

    it('should verify PPM 230-05 and SFI paragraph contents - test #06', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        expect(form800page.getF800Ppm230Text()).toContain(browser.params.Form800PPM230Message);
        expect(form800page.getF800SfiText()).toContain(browser.params.Form800SFIMessage);
    });

    it('should verify Form 800 Demographics section headings - test #07', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        expect(form800page.getF800LegendLabel()).toContain('Form 800');
        expect(form800page.getF800DemographicsLabel()).toContain('Demographics');
        expect(form800page.getF800PiNameLabel()).toContain('Name');
        expect(form800page.getF800PiNameFieldValue()).toContain('OR Tester');
        expect(form800page.getF800PiEmailLabel()).toContain('Email');
        expect(form800page.getF800PiEmailFieldValue()).toContain('trholmes@ucdavis.edu');
        expect(form800page.getF800PiDeptLabel()).toContain('Department');
        expect(form800page.getF800PiDeptFieldValue()).toContain('INFO SYSTEMS & SVCS - RESEARCH');
    });

    it('should verify Form 800 Project Details section headings - test #08', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        expect(form800page.getFrom800Label()).toContain('Form 800');
        expect(form800page.getF800ProjDropDownLabel()).toContain('Research Project');
        expect(form800page.getF800ProposalTypeLabel()).toContain('Type of Proposal / Disclosure');
        expect(form800page.getF800ProposalTypeText()).toContain('New Project');
        expect(form800page.getF800ProposalTypeText()).toContain('Continuation/Additional Funding');
        expect(form800page.getF800ProposalTypeText()).toContain('New Sponsor on Existing Project');
        expect(form800page.getF800ProposalTypeText()).toContain('Add Other Investigator to the Project');
        expect(form800page.getF800ProposalTypeText()).toContain('Change of Principal Investigator');
        expect(form800page.getF800ProposalTypeText()).toContain('Change in your existing financial interest or a newly discovered/acquired significant financial interest (SFI)');
        // expect(form800page.getF800ProposalTypeText()).toContain('IRB Annual update for continuing review (for investigators who previously submitted a positive Form 800)');

        expect(form800page.getF800PreviousAwardLabel()).toContain('Previous award number');
        expect(form800page.getF800PreviousAwardPlaceholderText()).toContain('(if applicable)');
        expect(form800page.getF800PreviousAwardTextbox()).toEqual('');

        expect(form800page.getF800FundingEntitySearchLabel()).toEqual('Funding Entity Search');
        expect(form800page.getF800FundingEntityPlaceholderText()).toContain('search sponsor name or code');
        expect(form800page.getF800FundingEntitySearchTextbox()).toEqual('');
        expect(form800page.getF800FundingSearchResultLabel()).toContain('Funding Entity');
        expect(form800page.getF800FundingTypeLabel()).toContain('Type of Funding Entity');

        expect(form800page.getF800BudgetFromLabel()).toContain('Budget Period From');
        expect(form800page.getF800BudgetToLabel()).toContain('Budget Period To');
        expect(form800page.getF800ProjectBeginDateLabel()).toContain('Project Begin Date');
        expect(form800page.getF800ProjectEndDateLabel()).toContain('Project End Date');
        expect(form800page.getF800FundingAmountLabel()).toContain('Funding Amount');
        expect(form800page.getF800FundingAmountPlaceholderText()).toContain('Amount of Funding');
        expect(form800page.getF800FundingAmountTextbox()).toMatch('');
        expect(form800page.getF800EstimateQuestionLabel()).toContain('Is this amount an estimate?');
        expect(form800page.getF800YesNoOptionText('estimate')).toContain('Yes');
        expect(form800page.getF800YesNoOptionText('estimate')).toContain('No');
        expect(form800page.getF800HumanSubectIrbLabel()).toContain('Human Subjects and IRB #');
        expect(form800page.getF800YesNoOptionText('IRB')).toContain('Yes');
        expect(form800page.getF800YesNoOptionText('IRB')).toContain('No');
        form800page.selectF800YesNoOption('#humanSubjects', 'YES');
        expect(form800page.getF800IrbNetIdLabel()).toContain('IRBNet ID');
        expect(form800page.getF800IrbPlaceholderText()).toContain('IRBNet ID (exclude suffix)');
        // form800page.f800EnterIRBNetId();
        // browser.sleep(5000);
        // expect(form800page.getF800IrbInputText()).toContain('1234567');

        expect(form800page.getF800HumanSubjectQuestionLabel()).toContain('Does this project involve Human Subjects?');
        expect(form800page.getF800YesNoOptionText('sfidisclosure')).toContain('Yes');
        expect(form800page.getF800YesNoOptionText('sfidisclosure')).toContain('No');

    });

    it('should verify Form 800 SFI and Signature section headings - test #09', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        expect(form800page.getF800SfiDisclosureLabel()).toContain('SFI Disclosure');
        expect(form800page.getF800SfiDisclosureText()).toContain(browser.params.Form800SFIDisclosureText);
        expect(form800page.getF800SignSubmitLabel()).toContain('Sign and submit');
        expect(form800page.getF800VerificationLabel()).toContain('Verification');
        expect(form800page.getF800VerificationText()).toContain(browser.params.Form800VerificationText);
        expect(form800page.getF800SignatureLabel()).toContain('Signature');
        expect(form800page.getF800CancelButtonExist()).toBe(true);
        expect(form800page.getF800SaveDraftButtonText()).toBe(true);
        expect(form800page.getF800FinalizeButtonText()).toBe(true);
    });


});

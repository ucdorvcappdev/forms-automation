import { Form800Page } from '../../pageObjects/forms800.po';
import { browser } from 'protractor';

describe('Form 800 Tests', () => {

    let form800page:  Form800Page;

    beforeEach(() => {
        form800page = new Form800Page();
     });


    it('should create a basic form 800 filing (New Project) - test #01', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        form800page.selectProposalType('New');
        form800page.f800selectProject();
        form800page.f800setFundingEntity('8282');
        browser.sleep(1000);
        expect(form800page.getf800EntityListText()).toContain('CHILDREN\'S HOSPITAL RESEARCH FOUNDATION--S.F. 8282')
        form800page.selectf800Entity('CHILDREN\'S HOSPITAL RESEARCH FOUNDATION--S.F. 8282');
        expect(form800page.getf800SelectedEntityText()).toContain('CHILDREN\'S HOSPITAL RESEARCH FOUNDATION--S.F.')
        form800page.selectF800SponsorType('Non-Public Health Service Federal entity (e.g. NSF)');
        form800page.setf800BudgetDateFrom();
        form800page.setf800BudgetDateTo();
        form800page.setf800ProjectDateFrom();
        form800page.setf800ProjectDateTo();
        form800page.setf800FundingAmount(70000.00);
        form800page.selectF800YesNoOption('#estimated','NO');
        form800page.selectF800YesNoOption('#humanSubjects','NO');
        form800page.selectF800YesNoOption('#sfi','NO');
        form800page.f800EnterSignature('Signed by Protractor Automation - ');
        expect(form800page.isF800FinalizeButtonEnabled()).toBe(true);
        // form800page.f800ClickFinalizeButton();
        browser.sleep(2000);
    });

    it('should create a basic form 800 filing (Continuation/Additional Funding) - test #02', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        form800page.selectProposalType('Continuation');
        form800page.setF800AdditionFundingAmount(55000);
        form800page.f800selectProject();
        form800page.f800setFundingEntity('8283');
        browser.sleep(1000);
        expect(form800page.getf800EntityListText()).toContain('CHABOT COLLEGE 8283')
        form800page.selectf800Entity('CHABOT COLLEGE 8283');
        expect(form800page.getf800SelectedEntityText()).toContain('CHABOT COLLEGE')
        form800page.selectF800SponsorType('Subaward from above federal entities from/through another entity');
        form800page.setf800BudgetDateFrom();
        form800page.setf800BudgetDateTo();
        form800page.setf800ProjectDateFrom();
        form800page.setf800ProjectDateTo();
        form800page.setf800FundingAmount(65000);
        browser.sleep(2000);
        form800page.selectF800YesNoOption('#estimated','YES');
        form800page.selectF800YesNoOption('#humanSubjects','YES');
        form800page.f800EnterIRBNetId(6);
        form800page.selectF800YesNoOption('#sfi','YES');
        form800page.f800EnterSignature('Signed by Protractor Automation - ');
        expect(form800page.f800SupplementalRequiredMessageIsDisplayed()).toBe(true);
        expect(form800page.getf800SupplementRequiredMessageText()).toContain(browser.params.SFISupplementalMessage);
        expect(form800page.isF800FinalizeButtonEnabled()).toBe(true);
        // form800page.f800ClickFinalizeButton();
        browser.sleep(4000);
    });

    it('should create a basic form 800 filing (New Sponsor on Existing Project) - test #03', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        form800page.selectProposalType('New Sponsor');
        form800page.setF800PriorSponsorInput('Bill & Melinda Gates Foundation');
        form800page.f800selectProject();
        form800page.f800setFundingEntity('8284');
        browser.sleep(1000);
        expect(form800page.getf800EntityListText()).toContain('CHANTAL PHARMACEUTICAL CORPORATION 8284')
        form800page.selectf800Entity('CHANTAL PHARMACEUTICAL CORPORATION 8284');
        expect(form800page.getf800SelectedEntityText()).toContain('CHANTAL PHARMACEUTICAL CORPORATION')
        form800page.selectF800SponsorType('Non-governmental Sponsor, Project involves Human Subjects');
        form800page.setf800BudgetDateFrom();
        form800page.setf800BudgetDateTo();
        form800page.setf800ProjectDateFrom();
        form800page.setf800ProjectDateTo();
        form800page.setf800FundingAmount(95000.00);
        form800page.selectF800YesNoOption('#estimated','NO');
        form800page.selectF800YesNoOption('#humanSubjects','YES');
        form800page.f800EnterIRBNetId(7);
        form800page.selectF800YesNoOption('#sfi','NO');
        form800page.f800EnterSignature('Signed by Protractor Automation - ');
        expect(form800page.isF800FinalizeButtonEnabled()).toBe(true);
        // form800page.f800ClickFinalizeButton();
        // browser.sleep(3000);
    });

    it('should create a basic form 800 filing (Add Other Investigator to the Project) - test #04', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        form800page.selectProposalType('Add Investigator');
        form800page.f800selectProject();
        form800page.f800setFundingEntity('8285');
        browser.sleep(1000);
        expect(form800page.getf800EntityListText()).toContain('CHOAY LABS, INC. 8285')
        form800page.selectf800Entity('CHOAY LABS, INC. 8285');
        expect(form800page.getf800SelectedEntityText()).toContain('CHOAY LABS, INC.')
        form800page.selectF800SponsorType('California Institute of Regenerative Medicine (CIRM)');
        form800page.setf800BudgetDateFrom();
        form800page.setf800BudgetDateTo();
        form800page.setf800ProjectDateFrom();
        form800page.setf800ProjectDateTo();
        form800page.setf800FundingAmount(10500.00);
        form800page.selectF800YesNoOption('#estimated','YES');
        form800page.selectF800YesNoOption('#humanSubjects','NO');
        form800page.selectF800YesNoOption('#sfi','NO');
        form800page.f800EnterSignature('Signed by Protractor Automation - ');
        expect(form800page.isF800FinalizeButtonEnabled()).toBe(true);
        // form800page.f800ClickFinalizeButton();
        // browser.sleep(3000);
    });

    it('should create a basic form 800 filing (Change of Principal Investigator) - test #05', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        form800page.selectProposalType('Change PI');
        form800page.setF800PriorPINameInput('Johannes Hell');
        form800page.f800selectProject();
        form800page.f800setFundingEntity('8286');
        browser.sleep(1000);
        expect(form800page.getf800EntityListText()).toContain('NOVARTIS VACCINES & DIAGNOSTICS, INC. 8286')
        form800page.selectf800Entity('NOVARTIS VACCINES & DIAGNOSTICS, INC. 8286');
        expect(form800page.getf800SelectedEntityText()).toContain('NOVARTIS VACCINES & DIAGNOSTICS, INC.')
        form800page.selectF800SponsorType('Department Funded (if the project is FDA regulated)');
        form800page.setf800BudgetDateFrom();
        form800page.setf800BudgetDateTo();
        form800page.setf800ProjectDateFrom();
        form800page.setf800ProjectDateTo();
        form800page.setf800FundingAmount(50000.00);
        form800page.selectF800YesNoOption('#estimated','YES');
        form800page.selectF800YesNoOption('#humanSubjects','NO');
        form800page.selectF800YesNoOption('#sfi','NO');
        form800page.f800EnterSignature('Signed by Protractor Automation - ');
        expect(form800page.isF800FinalizeButtonEnabled()).toBe(true);
        // form800page.f800ClickFinalizeButton();
        browser.sleep(2000);
    });

    it('should create a basic form 800 filing (Change in your existing financial interest or a newly discovered/acquired significant financial interest (SFI)) - test #06', function() {
        form800page.navigateTo();
        form800page.clickOnCreate800FormLink();
        expect(form800page.getBuildForm800URL()).toEqual(browser.baseUrl + 'buildForm800');
        form800page.selectProposalType('New SFI');
        form800page.f800selectProject();
        form800page.f800setFundingEntity('8288');
        browser.sleep(1500);
        expect(form800page.getf800EntityListText()).toContain('BEVERLY HILLS EDUCATION FOUNDATION 8288')
        form800page.selectf800Entity('BEVERLY HILLS EDUCATION FOUNDATION 8288');
        expect(form800page.getf800SelectedEntityText()).toContain('BEVERLY HILLS EDUCATION FOUNDATION')
        form800page.selectF800SponsorType('Department Funded (if the project is FDA regulated)');
        form800page.setf800BudgetDateFrom();
        form800page.setf800BudgetDateTo();
        form800page.setf800ProjectDateFrom();
        form800page.setf800ProjectDateTo();
        form800page.setf800FundingAmount(84000.00);
        form800page.selectF800YesNoOption('#estimated','YES');
        form800page.selectF800YesNoOption('#humanSubjects','NO');
        form800page.selectF800YesNoOption('#sfi','NO');
        form800page.f800EnterSignature('Signed by Protractor Automation - ');
        expect(form800page.isF800FinalizeButtonEnabled()).toBe(true);

        // form800page.f800ClickFinalizeButton();
        browser.sleep(3000);
    });

});
import { FormPhsPage } from '../../pageObjects/formsPhs.po';
import { browser } from 'protractor';


describe('Form PHS Tests: UI Element verification', () => {

    let formPhsPage:  FormPhsPage;

    beforeEach(() => {
        formPhsPage = new FormPhsPage();
     });

    it('should display form PHS tab - test #01', function() {
        formPhsPage.navigateTo();
        browser.sleep(500);
        expect(formPhsPage.getPhsUrlText()).toContain(browser.baseUrl+'landing?tab=formPHS');
        expect(formPhsPage.getFormPhsText()).toContain('Public Health Service');
    });

    it('should verify form PHS table headers - test #02', function() {
        formPhsPage.navigateTo();
        expect(formPhsPage.getPhsTableHeaderText()).toContain('Investigator Signature Reason Filed Status FCOI Status Date Submitted PHS Form MP');
    });

    it('should verify Create Disclosure Text and link - test #03', function() {
        formPhsPage.navigateTo();
        // startPhsFiling function checks for the existance of a Draft first,
        // if it doesn't exist, then it finds the Create Form link to start a new form.
        formPhsPage.startPhsFiling();
        browser.sleep(1000);
        expect(formPhsPage.getPhsGlossaryFormLinkText()).toContain('Glossary of Terms')
        expect(formPhsPage.getPhsGlossaryText()).toContain(browser.params.FormPHSGlossary);
        formPhsPage.clickPhsCancelButton();

    });

    it('should verify Section 1 of the PHS form - test #04', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(1000);
        expect(formPhsPage.getPhsInvestigatorInfoLabel()).toContain('I. Investigator Information');
        expect(formPhsPage.getPhsNameLabel()).toContain('Name:');
        expect(formPhsPage.getPhsInvestigatorText()).toContain('OR Tester');
        expect(formPhsPage.getPhsEmailLabel()).toContain('Email:');
        expect(formPhsPage.getPhsEmailText()).toContain('trholmes@ucdavis.edu');
        expect(formPhsPage.getPhsDepartmentLabel()).toContain('Department:');
        expect(formPhsPage.getPhsDepartmentText()).toContain('Info systems & svcs - research');
        formPhsPage.clickPhsCancelButton();
    });

    it('should verify Section II of the PHS form - test #05', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(1000);
        expect(formPhsPage.getPhsCertificationLabel()).toContain('II. Training Certification');
        expect(formPhsPage.getPhsTrainedQuestionText()).toContain('Have you completed the Financial Conflict of Interest ("FCOI") Training required by PHS?');
        expect(formPhsPage.isCompletedTrainingOptionsDisplayed()).toBe(true);
        formPhsPage.selectPhsYesNoOption('completedTraining','YES');
        expect(formPhsPage.isPhsTrainingInfoTextDisplayed()).toBe(false);
        formPhsPage.selectPhsYesNoOption('completedTraining','NO');
        expect(formPhsPage.isPhsTrainingInfoTextDisplayed()).toBe(true);
        expect(formPhsPage.getPhsTrainingInfoText()).toContain(browser.params.FormPHSTrainingInfo);
        expect(formPhsPage.getPhsTrainingListText()).toContain(browser.params.FormPHSTrainingList);
        expect(formPhsPage.getPhsTrainingText()).toContain('Please complete your training as soon as possible to avoid any funding delays.');
        formPhsPage.clickPhsCancelButton();
    });

    it('should verify Section III of the PHS form - test #06', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(1000);
        expect(formPhsPage.getPhsDisclosureLabel()).toContain('III. Disclosure');
        expect(formPhsPage.getPhsDisclosureReasonLabel()).toContain('Reason for Disclosure:');
        expect(formPhsPage.isPhsDisclosureReasonOptionsDisplayed()).toBe(true);
        expect(formPhsPage.getPhsDisclosureReasonOptionsText()).toContain('Initial\nAnnual update of existing Disclosure\nAdd a new project or add a newly discovered/acquired Significant Financial Interest (SFI)\nCorrection to existing Disclosure');
        formPhsPage.clickPhsCancelButton();
    });

    it('should verify Section IV of the PHS form - test #07', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(1000);
        expect(formPhsPage.getPhsSection4Label()).toContain('IV. Income and Equity [This section relates to INCOME you have received from an outside entity and/or any EQUITY you have in the outside entity]');
        expect(formPhsPage.getPhsIncomeNoteText()).toContain('Note: Any income or equity (publicly traded or non-publicly traded) related or not to your PHS research, should be included.');
        expect(formPhsPage.getPhsWizardLabel()).toContain('Income/Equity SFI Wizard');
        expect(formPhsPage.getPhsWizardEntityLabel()).toContain('Outside Entity [Select or type name of outside entity]:');
        ;
        browser.sleep(1000);
        expect(formPhsPage.getPhsEntityDropDownText()).toContain('Select');
        expect(formPhsPage.getPhsEntityDropDownText()).toContain('Create new...');

        formPhsPage.selectPhsEntity('Create new...');    //This is the Creat new... drop-down option
        browser.sleep(500);
        expect(formPhsPage.getPhsNewEntityTextboxPlaceholderText()).toContain('Enter new entity');
        browser.sleep(500);
        expect(formPhsPage.getPhsWizardEntityTypeLabel()).toContain('Type of Entity [Select whether the entity is publicly traded or non-publicly traded]:');
        expect(formPhsPage.getPhsEntityTypeDropDownText()).toContain('Select');
        formPhsPage.selectPhsEntityTypeList('Publicly traded entity');
        expect(formPhsPage.getPhsEntityTypeDropDownText()).toContain('Publicly traded entity');
        formPhsPage.selectPhsEntityTypeList('Non-publicly traded entity');
        expect(formPhsPage.getPhsEntityTypeDropDownText()).toContain('Non-publicly traded entity');

        expect(formPhsPage.getPhsIncomeTextboxPlaceholder()).toContain('Enter zero if none')
        expect(formPhsPage.getPhsIncomeGlossaryText()).toContain(browser.params.FormPHSIncomeGlossary);
        expect(formPhsPage.getPhsEquityGlossaryText()).toContain(browser.params.FormPHSEquityGlossary);
        expect(formPhsPage.getPhsAddSfiButtonText()).toContain('Add SFI');
        expect(formPhsPage.getPhsSfiTableHeader()).toContain('SFI Type Entity Name Recipient Dollar Range SFI Opinion Action');
        expect(formPhsPage.getPhsNoSfiReportedMessage()).toContain('No public/non-public SFIs reported.');
        browser.sleep(500);
        formPhsPage.clickPhsCancelButton();
    });

    it('should verify Section V of the PHS form - test #08', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(1000);
        expect(formPhsPage.getPhsSection5Label()).toContain('V. Intellectual Property');
        expect(formPhsPage.getPhsIPGlossaryText()).toContain(browser.params.FormPHSIPGlossary);
        expect(formPhsPage.getPhsIPNoteText()).toContain(browser.params.FormPHSIPNote);
        expect(formPhsPage.getPhsIPTableHeaderText()).toContain('SFI Type Entity Name Recipient Dollar Range SFI Opinion Action');
        formPhsPage.clickPhsCancelButton();
    });

    it('should verify Section VI of the PHS form - test #09', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(1000);
        expect(formPhsPage.getPhsTravelSectionLabel()).toContain('VI. Travel');
        expect(formPhsPage.getPhsTravelGlossaryText()).toContain(browser.params.FormPHSTravelGlossary);
        expect(formPhsPage.getPhsTravelTableHeader()).toContain('SFI Type Travel Location Begin Date End Date Action');
        expect(formPhsPage.getPhsTravelNoteText()).toContain(browser.params.FormPHSTravelNote);
        // browser.sleep(2500);
        formPhsPage.clickPhsCancelButton();
    });

    it('should verify Section VII of the PHS form - test #010', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(1000);
        expect(formPhsPage.getPhsOutsideActivitySectionLabel()).toContain('VII. Other Outside Activity');
        expect(formPhsPage.getPhsOutsideActivityGlossaryText()).toContain(browser.params.FormPHSOutsideActivityGlossary);
        expect(formPhsPage.getPhsManagementTableHeader()).toContain('SFI Type Position Responsibilities Action');
        // expect(formPhsPage.getPhsTravelNoteText()).toContain(browser.params.FormPHSTravelNote);
        // browser.sleep(2500);
        formPhsPage.clickPhsCancelButton();
    });

    it('should verify Section VIII of the PHS form - test #011', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(1000);
        expect(formPhsPage.getPhsS8ResearchProjectLabel()).toContain('VIII. Research Projects (only required if a SFI was reported above.)');
        expect(formPhsPage.getPhsResearchProjectLabel()).toContain('Research Projects');
        expect(formPhsPage.getPhsResearchProjectTableHeader()).toContain('Project Title Role Project Start Project End Funding Agency Human Subjects?');
        expect(formPhsPage.getPhsManageProjectLinkText()).toContain('Manage PHS Projects');
        // expect(formPhsPage.getPhsTravelNoteText()).toContain(browser.params.FormPHSTravelNote);
        // browser.sleep(2500);
        formPhsPage.clickPhsCancelButton();
    });

    it('should verify Section IX of the PHS form - test #012', function() {
            formPhsPage.navigateTo();
            formPhsPage.startPhsFiling();
            browser.sleep(1000);
            expect(formPhsPage.getPhsS9SignatureLabel()).toContain('IX. Signature and submission');
            expect(formPhsPage.getPhsSignatureGlossaryText()).toContain(browser.params.FormPHSSignatureAcknowledement);
            expect(formPhsPage.getPhsSignatureTextboxLabel()).toContain('Signature');
            // expect(formPhsPage.getPhsManageProjectLinkText()).toContain('Manage PHS Projects');
            // expect(formPhsPage.getPhsTravelNoteText()).toContain(browser.params.FormPHSTravelNote);
            // browser.sleep(2500);
            formPhsPage.clickPhsCancelButton();
        });

        it('should verify Section Income SFI modal window of the PHS form (publicly traded modal)- test #013a', function() {
            formPhsPage.navigateTo();
            formPhsPage.startPhsFiling();
            browser.sleep(1000);

            formPhsPage.selectPhsEntity('TEST');
            expect(formPhsPage.getPhsEntityDropDownText()).toContain('TEST');
            formPhsPage.selectPhsEntityTypeList('Publicly traded entity');
            formPhsPage.setPhsIncomeAmount(8000);
            formPhsPage.clickPhsAddIncomeSfiButton();
            browser.sleep(200);

            //Modal test start here
            expect(formPhsPage.getPhsModalIncomeEntitylabel()).toContain('SFI Disclosure for Publicly Traded Entity');
            expect(formPhsPage.getPhsModalIncomeEntityTitleText()).toContain('Entity:');
            expect(formPhsPage.getPhsModalIncomeEntityValue()).toContain('TEST');
            expect(formPhsPage.getPhsModalIncomeEntityTypeValue()).toContain('Publicly traded entity');
            expect(formPhsPage.getPhsModalIncomeGlossary()).toContain(browser.params.FormPHSIncomeModalIncomeGlossary);
            // expect(formPhsPage.getPhsModalIncomeValue()).toContain('8000');
            expect(formPhsPage.getPhsModalIncomeEquityGlossary()).toContain(browser.params.FormPHSIncomeModalEqutiyGlossary);
            // expect(formPhsPage.getPhsModalIncomeEquityValue()).toMatch('0');
            expect(formPhsPage.getPhsModalIncomeStatementTypeLabel()).toContain('Individual(s) who received income and/or holds equity interest:');
            expect(formPhsPage.getPhsModalIncomeRelationshipOptionsText()).toContain('Select\nSelf\nSpouse/Registered Domestic Partner\nDependent Children\nSelf + Immediate Family Member(s)');
            expect(formPhsPage.getPhsModalIncomeTableHeaders())
                    .toContain('Consulting\nDistribution/Dividends\nHonoraria\nPaid Authorship\nPayment in Kind\nPer Diem\nSalary\nStipend\nOther\nAmount');
            expect(formPhsPage.getPhsModalIncomeDollarsHelpText()).toContain('Total dollars entered:$0.00(must match total income reported above).');
            expect(formPhsPage.getPhsModalIncomePurposeGlossary()).toContain('Provide a description of the business purpose of');
            expect(formPhsPage.getPhsModalIncomeReasonableGlossary()).toContain(browser.params.FormPHSIncomeModalReasonableGlossary);
            expect(formPhsPage.getPhsModalIncomeStatementGlossary()).toContain(browser.params.FormPHSIncomeModalPublicStatementGlossary);
            expect(formPhsPage.isPhsIncomeModalCancelButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsIncomeModalSubmitButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsIncomeModalDeleteButtonDisplayed()).toBe(false);
            expect(formPhsPage.isPhsIncomeModalConfirmDeleteButtonDisplayed()).toBe(false);
        });

        it('should verify Section Income SFI modal window of the PHS form (non-publicly traded modal)- test #013b', function() {
            formPhsPage.navigateTo();
            formPhsPage.startPhsFiling();
            browser.sleep(1000);

            formPhsPage.selectPhsEntity('TEST');
            expect(formPhsPage.getPhsEntityDropDownText()).toContain('TEST');
            formPhsPage.selectPhsEntityTypeList('Non-publicly traded entity');
            formPhsPage.setPhsIncomeAmount(9000);
            formPhsPage.clickPhsAddIncomeSfiButton();
            browser.sleep(200);

            //Modal test start here
            expect(formPhsPage.getPhsIncomeModalNPTitle())
                .toContain('SFI Disclosure for Non-Publicly Traded Entity');
            expect(formPhsPage.getPhsIncomeModalNPEntityLabel()).toContain('Entity:');
            expect(formPhsPage.getPhsIncomeModalNPEntityValue()).toContain('TEST');
            expect(formPhsPage.getPhsIncomeModalNPEntityTypeLabel())
                .toContain('Type of Entity');
            expect(formPhsPage.getPhsIncomeModalNPEntityTypeValue())
                .toContain('Non-publicly traded entity');
            expect(formPhsPage.getPhsIncomeModalNPGlossary())
                .toContain(browser.params.FormPHSIncomeModalNPResponsabilitiesGlossary);
            expect(formPhsPage.getPhsIncomeModalNPStatementTypeLabel())
                .toContain('Individual(s) who received income and/or holds equity interest:');
            expect(formPhsPage.getPhsIncomeModalNPRelationshiptSelectList())
                .toContain('Select\nSelf\nSpouse/Registered Domestic Partner\nDependent Children\nSelf + Immediate Family Member(s)');
            expect(formPhsPage.getPhsIncomeModalNPPurposeLabel())
                .toContain('Purpose of Income');
            expect(formPhsPage.getPhsIncomeModalNPItemizeTableHeaders())
                .toContain('Consulting\nDistribution/Dividends\nHonoraria\nPaid Authorship\nPayment in Kind\nPer Diem\nSalary\nStipend\nOther\nAmount');

            expect(formPhsPage.getPhsIncomeModalNPTotalDollarsLabel())
                .toContain('Total dollars entered:$0.00(must match total income reported above).');
            expect(formPhsPage.getPhsIncomeModalNPReasonableQuestion())
                .toContain(browser.params.FormPHSIncomeModalReasonableGlossary);
            expect(formPhsPage.getPhsIncomeModalNPPurposeQuestion())
                .toContain('Provide a description of the business purpose of');
            expect(formPhsPage.getPhsIncomeModalNPStatementQuestion())
                .toContain(browser.params.FormPHSIncomeModalPublicStatementGlossary);
            expect(formPhsPage.isPhsIncomeModalNPCancelButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsIncomeModalNPSubmitButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsIncomeModalNPDeleteButtonDisplayed()).toBe(false);
            expect(formPhsPage.isPhsIncomeModalNPDeleteConfirmButtonDisplayed()).toBe(false);
        });

        it('should verify Section Intellectual Property SFI modal window of the PHS form - test #014', function() {
            formPhsPage.navigateTo();
            formPhsPage.startPhsFiling();
            browser.sleep(1000);
            formPhsPage.selectPhsYesNoOption('ipRights','YES');
            formPhsPage.clickPhsAddSfi('ipRights');
            browser.sleep(200);

            // Modal window
            expect(formPhsPage.getPhsIPModalTitleText()).toContain('SFI Disclosure for Intellectual Property Rights');
            expect(formPhsPage.getPhsIPModalEntityLabel()).toContain('Entity:');
            expect(formPhsPage.getPhsIpModalTotalAmountLabel()).toContain('Total amount received from this entity:');
            expect(formPhsPage.getPhsIpModalTotalAmountSelectListText()).toContain(browser.params.FormPHSIPModalTotalAmountRange);
            expect(formPhsPage.getPhsIpModalRelationshipLabel()).toContain('');
            expect(formPhsPage.getPhsIpModalRelationshipSelectListText()).toContain(browser.params.FormPHSIPModalRelationshipList);
            expect(formPhsPage.getPhsIpModalIpDescriptionGlossary()).toContain(browser.params.FormPHSIPModalRelationshipGlossary);
            expect(formPhsPage.getPhsIpModalOpinionGlossary()).toContain(browser.params.FormPHSIPModalOpinionGlossary);
            expect(formPhsPage.getPhsIpModalStatementGlossary()).toContain(browser.params.FormPHSIPModalStatementGlossary);

            expect(formPhsPage.isPhsIpModalCancelButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsIpModalSubmitButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsIpModalSubmitAgainButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsIpModalDeleteButtonDisplayed()).toBe(false);
            expect(formPhsPage.isPhsIpModalDeleteConfirmButtonDisplayed()).toBe(false);
        });

        it('should verify Section Travel SFI modal window of the PHS form - test #015', function() {
            formPhsPage.navigateTo();
            formPhsPage.startPhsFiling();
            browser.sleep(1000);
            formPhsPage.selectPhsYesNoOption('travel','YES');
            formPhsPage.clickPhsAddSfi('travel');
            browser.sleep(200);

            expect(formPhsPage.getPhsTravelModalTitleText())
                .toContain('SFI Disclosure for Travel');
            expect(formPhsPage.getPhsTravelModalTravelNoticeText())
                .toContain(browser.params.FormPHSTravelModalNote);
            expect(formPhsPage.getPhsTravelModal5KNoteText())
                .toContain(browser.params.FormPHSTravelModal5KNote);
            expect(formPhsPage.getPhsTravelModalEntityLabel())
                .toContain('Entity:');
            expect(formPhsPage.getPhsTravelModalEntitySelectListText())
                .toContain('Select');
            expect(formPhsPage.getPhsTravelModalEntitySelectListText())
                .toContain('Create new...');
            expect(formPhsPage.getPhsTravelDateLabel()).toContain('Travel Begin\nand End Dates');
            formPhsPage.setPhsTravelModalBeginTravelDate('04/01/2020');
            formPhsPage.setPhsTravelModalEndTravelDate('04/07/2020');
            expect(formPhsPage.getPhsTravelModalTravelReasonQuestion())
                .toContain('Reason for anticipated travel ( e.g. reimbursement for travel expenses to present lectures and posters about UCD research in Bangkok).');
            expect(formPhsPage.getPhsTravelModalReasonableGlossary())
                .toContain(browser.params.FormTravelModalReasonableQuestGlosssary);
            expect(formPhsPage.getPhsTravelModalStatementGlossary())
                .toContain(browser.params.FormTravelModalStatementGlossary);
            expect(formPhsPage.isPhsTravelModalCancelButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsTravelModalSubmitButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsTravelModalSubmitAgainButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsTravelModalDeleteButtonDisplayed()).toBe(false);
            expect(formPhsPage.isPhsTravelModalConfirmDeleteButtonDisplayed()).toBe(false);

        });

        it('should verify Section Management Position SFI modal window of the PHS form - test #016', function() {
            formPhsPage.navigateTo();
            formPhsPage.startPhsFiling();
            browser.sleep(1000);
            formPhsPage.selectPhsYesNoOption('management','YES');
            formPhsPage.clickPhsAddSfi('management');
            browser.sleep(200);

            expect(formPhsPage.getPhsManagementModalSfiTitle()).toContain('SFI Disclosure for Management');
            expect(formPhsPage.getPhsManagementModalPositionLabel()).toContain('Position:');
            expect(formPhsPage.getPhsManagementModalPositionSelectListText())
                .toContain('Select\nDirector\nPartner\nMember Board of Directors\nMember Scientific Advisory Board\nOfficer\nTrustee\nOther');

            expect(formPhsPage.getPhsManagementModalEntityGlossary())
                .toContain(browser.params.FormPHSManagementEntityGlossary);
            expect(formPhsPage.isPhsManagementModalCancelButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsManagementModalSubmitButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsManagementModalSubmitAnotherButtonDisplayed()).toBe(true);
            expect(formPhsPage.isPhsManagementModalDeleteButtonDisplayed()).toBe(false);
            expect(formPhsPage.isPhsManagementModalDeleteConfirmDisplayed()).toBe(false);

        });

});

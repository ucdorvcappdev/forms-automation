import { FormPhsPage } from '../../pageObjects/formsPhs.po';
import { browser } from 'protractor';

describe('Form PHS Tests: Basic filings of PHS forms', () => {
    let formPhsPage:  FormPhsPage;

    beforeEach(() => {
        formPhsPage = new FormPhsPage();
     });


     it('should delete all public income SFIs - test #01', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.deletePhsIncomePublicSfiFiling();
        browser.sleep(200);
     });

    it('should check that there are no SFIs to delete - test #02', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.deletePhsIncomePublicSfiFiling();
        browser.sleep(200);

     });

     it('should delete all non-public income SFI filings - test #03', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.deletePhsIncomeNonPublicSfiFiling();
        browser.sleep(750);
     });

     it('should delete all non-public income and equity SFI filings - test #04', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.deletePhsIncomeNonPublicSfiFiling();
        browser.sleep(200);
        formPhsPage.deletePhsIncomeNonPublicEquitySfiFiling()
     });

    it('should delete all filed Intellectual Property SFIs - test #05', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.deletePhsIpSfiFiling();
        browser.sleep(200);
    });

    it('should delet all filed Travel SFI to file - test #06', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.deletePhsTravelSfiFiling();
    });

    it('should delete all Outside Acitivity SFIs - test #07', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.deletePhsOutsideActivitySfiFiling();
        browser.sleep(750);
    });

});
import { FormPhsPage } from '../../pageObjects/formsPhs.po';
import { browser } from 'protractor';

describe('Form PHS Tests: Error messages verification', () => {
    let formPhsPage:  FormPhsPage;

    beforeEach(() => {
        formPhsPage = new FormPhsPage();
     });


     it('should display three errors when user submits PHS form w/o filling in details - test #01', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.setPhsSignature();
        formPhsPage.clickPhsSubmitButton();
        browser.sleep(250);

        expect(formPhsPage.getPhsDocumentHasErrorMessage())
            .toContain('! - Document not saved. Errors exist on page.');
        expect(formPhsPage.getPhsStatementTypeErrorMessage())
            .toContain('Indicate a statement type');
        expect(formPhsPage.getPhsDocumentNotSavedErrorMessage())
            .toContain('! - Document not saved. Errors exist on page.');
    });

     it('should display an error when user adds an income SFI w/o filling in details - test #02', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.clickPhsAddIncomeSfiButton();
        expect(formPhsPage.getPhsIncomeIncompleteErrorMessage())
            .toContain('All fields require a valid entry/selection');
    });

     it('should display errors when a user submits an income SFI w/o filling in the details on a publicly traded entity- test #03', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.selectPhsEntity('TEST');
        expect(formPhsPage.getPhsEntityDropDownText()).toContain('TEST');
        formPhsPage.selectPhsEntityTypeList('Publicly traded entity');
        formPhsPage.setPhsIncomeAmount(8000);
        formPhsPage.clickPhsAddIncomeSfiButton();
        browser.sleep(200);

        formPhsPage.clickPhsIncomeModalSubmitButton();
        expect(formPhsPage.getPhsIncomeModalRelationshipErrorMessage())
            .toContain('You must indicate an individual to whom the interest applies.');
        expect(formPhsPage.getPhsIncomeModalAllFieldsRequiredError())
            .toContain('All fields require a value and must total to the reported income.');
        expect(formPhsPage.getPhsIncomeModalPurposeErrorMessage())
            .toContain('This question requires an answer.');
        expect(formPhsPage.getPhsIncomeModalReasonableErrorMessage())
            .toContain('This question requires an answer.');
        expect(formPhsPage.getPhsIncomeModalStatementErrorMessage())
            .toContain('This question requires an answer.');
    });

     it('should display errors when a user submits an income SFI w/o filling in the details on a non-publicly traded entity- test #03', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.selectPhsEntity('TEST');
        expect(formPhsPage.getPhsEntityDropDownText()).toContain('TEST');
        formPhsPage.selectPhsEntityTypeList('Non-publicly traded entity');
        formPhsPage.setPhsIncomeAmount(8800);
        formPhsPage.clickPhsAddIncomeSfiButton();
        browser.sleep(200);

        formPhsPage.clickPhsIncomeModalNPSubmitButton();
        // browser.sleep(5000);
        expect(formPhsPage.getPhsIncomeModalNPRelationshipListError())
            .toContain('You must indicate an individual to whom the interest applies.');
        expect(formPhsPage.getPhsIncomeModalNPAllFieldsRequiredError())
            .toContain('All fields require a value and must total to the reported income.');
        expect(formPhsPage.getPhsIncomeModalNPPurposeError())
            .toContain('This question requires an answer.');
        // Error message check below has been disable pending a fix. fixed 05/01/2020
        expect(formPhsPage.getPhsIncomeModalNPReasonableError())
            .toContain('This question requires an answer.');
        expect(formPhsPage.getPhsIncomeModalNPStatementError())
            .toContain('This question requires an answer.');
    });

    it('should verify error messages in the Intellectual Property SFI modal - test #004', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(1000);
        formPhsPage.selectPhsYesNoOption('ipRights','YES');
        formPhsPage.clickPhsAddSfi('ipRights');
        browser.sleep(200);

        formPhsPage.clickPhsIpModalSubmitButton();
        expect(formPhsPage.getPhsIPModalEntityError())
            .toContain('You must select or add an entity.');
        expect(formPhsPage.getPhsIPModalAmountError())
            .toContain('You must indicate the total amount received from this entity.');
        expect(formPhsPage.getPhsIPModalReplationshipError())
            .toContain('You must indicate an individual to whom the interest applies.');
        expect(formPhsPage.getPhsIpModalDescriptionError())
            .toContain('This question requires an answer.');
        expect(formPhsPage.getPhsIpModalReasonableError())
            .toContain('This question requires an answer.');
        expect(formPhsPage.getPhsIpModalStatmenetError())
            .toContain('This question requires an answer.');
    });

    it('should verify error messages in the Travel Modal window - test #005', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(1000);
        formPhsPage.selectPhsYesNoOption('travel','YES');
        formPhsPage.clickPhsAddSfi('travel');
        browser.sleep(200);

        formPhsPage.clickPhsTravelModalSubmitButton();
        expect(formPhsPage.getPhsTravelModalEntityError())
            .toContain('You must select or add an entity.');
        expect(formPhsPage.getPhsTravelModalDestinationError())
            .toContain('A destination is required');
        expect(formPhsPage.getPhsTravelModalTravelDateError())
            .toContain('Both a begin and end date are required.', 'Date formats must be MM/DD/YYYY.');
        expect(formPhsPage.getPhsTravelModalTravelReasonError())
            .toContain('This question requires an answer.');
        expect(formPhsPage.getPhsTravelModalTravelReasonableError())
            .toContain('This question requires an answer.');
        expect(formPhsPage.getPhsTravelModalTravelStatementError())
            .toContain('This question requires an answer.');

    });

    it('should verify error messages in the Management Modal window - test #006', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(1000);
        formPhsPage.selectPhsYesNoOption('management','YES');
        formPhsPage.clickPhsAddSfi('management');
        browser.sleep(200);

        formPhsPage.clickPhsManagementModalSubmitButton();
        expect(formPhsPage.getPhsManagementModalPositionError())
            .toContain('A position is required.');
        expect(formPhsPage.getPhsManagementModalResponsabilitiesError())
            .toContain('This question requires an answer.');

    });
});
import { FormPhsPage } from '../../pageObjects/formsPhs.po';
import { browser } from 'protractor';

describe('Form PHS Tests: Basic filings of PHS forms', () => {
    let formPhsPage:  FormPhsPage;

    beforeEach(() => {
        formPhsPage = new FormPhsPage();
     });

     it('should file a basic disclousre with No SFIs to file - test #01', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.selectPhsYesNoOption('completedTraining','NO');
        formPhsPage.selectPhsDisclosureType('Initial');
        formPhsPage.selectPhsYesNoOption('ipRights','NO');
        formPhsPage.selectPhsYesNoOption('travel','NO');
        formPhsPage.selectPhsYesNoOption('management','NO');
        formPhsPage.setPhsSignature();
        formPhsPage.clickPhsSubmitButton();
        browser.sleep(500);
    });

     it('should file a basic disclousre with just the public entity income SFI - test #02a', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.selectPhsYesNoOption('completedTraining','YES');
        formPhsPage.selectPhsDisclosureType('Initial');
        formPhsPage.selectPhsEntity('TEST');
        expect(formPhsPage.getPhsEntityDropDownText()).toContain('TEST');
        formPhsPage.selectPhsEntityTypeList('Publicly traded entity');
        formPhsPage.setPhsIncomeAmount(15000);
        formPhsPage.clickPhsAddIncomeSfiButton();
        browser.sleep(500);

        formPhsPage.selectPhsIncomeModalRelationshipList('Self')
        formPhsPage.setPhsIncomeModalIncomeList('Consulting','5000');
        formPhsPage.setPhsIncomeModalIncomeList('Stipend','5000');
        formPhsPage.setPhsIncomeModalIncomeList('Per Diem','5000');
        expect(formPhsPage.getPhsIncomeModalAccumulatorTotal()).toEqual('$15,000.00');
        formPhsPage.setPhsIncomeModalBusinessPurposeStatement();
        expect(formPhsPage.getPhsIncomeModalBusinessPurposeWordCount())
            .toBe('1962 characters remaining');
        formPhsPage.selectPhsYesNoOption('sfiRelated','NO');
        formPhsPage.setPhsIncomeModalBusinessReasonableStatement();
        expect(formPhsPage.getPhsIncomeModalReasonableWordCount())
            .toBe('1962 characters remaining');
        // formPhsPage.clickPhsIncomeModalCancelButton();
        formPhsPage.clickPhsIncomeModalSubmitButton();
        browser.sleep(500);
        formPhsPage.selectPhsYesNoOption('ipRights','NO');
        formPhsPage.selectPhsYesNoOption('travel','NO');
        formPhsPage.selectPhsYesNoOption('management','NO');
        formPhsPage.setPhsSignature();
        formPhsPage.clickPhsSubmitButton();

     });
     it('should file a basic disclousre with public entity income and equity SFI - test #02b', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.selectPhsYesNoOption('completedTraining','YES');
        formPhsPage.selectPhsDisclosureType('New Project');
        formPhsPage.selectPhsEntity('TEST');
        expect(formPhsPage.getPhsEntityDropDownText()).toContain('TEST');
        formPhsPage.selectPhsEntityTypeList('Publicly traded entity');
        formPhsPage.setPhsIncomeAmount(25000);
        formPhsPage.selectPhsYesNoOption('equityInterest','YES');
        formPhsPage.setPhsIncomeEquityInterestAmount(10000);
        formPhsPage.clickPhsAddIncomeSfiButton();
        browser.sleep(500);

        formPhsPage.selectPhsIncomeModalRelationshipList('Spouse/Registered Domestic Partner');
        formPhsPage.setPhsIncomeModalIncomeList('Consulting','10000');
        formPhsPage.setPhsIncomeModalIncomeList('Dividends','5000');
        formPhsPage.setPhsIncomeModalIncomeList('Per Diem','5000');
        formPhsPage.setPhsIncomeModalIncomeList('In Kind','5000');
        expect(formPhsPage.getPhsIncomeModalAccumulatorTotal()).toEqual('$25,000.00');
        formPhsPage.setPhsIncomeModalBusinessPurposeStatement();
        expect(formPhsPage.getPhsIncomeModalBusinessPurposeWordCount())
            .toBe('1962 characters remaining');
        formPhsPage.selectPhsYesNoOption('sfiRelated','NO');
        formPhsPage.setPhsIncomeModalBusinessReasonableStatement();
        expect(formPhsPage.getPhsIncomeModalReasonableWordCount())
            .toBe('1962 characters remaining');
        // formPhsPage.clickPhsIncomeModalCancelButton();
        formPhsPage.clickPhsIncomeModalSubmitButton();
        browser.sleep(500);
        formPhsPage.selectPhsYesNoOption('ipRights','NO');
        formPhsPage.selectPhsYesNoOption('travel','NO');
        formPhsPage.selectPhsYesNoOption('management','NO');
        formPhsPage.setPhsSignature();
        formPhsPage.clickPhsSubmitButton();

     });

     it('should file a basic disclousre with just the non-public entity income SFI - test #03a', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.selectPhsYesNoOption('completedTraining','YES');
        formPhsPage.selectPhsDisclosureType('Initial');
        formPhsPage.selectPhsEntity('TEST');
        expect(formPhsPage.getPhsEntityDropDownText()).toContain('TEST');
        formPhsPage.selectPhsEntityTypeList('Non-publicly traded entity');
        formPhsPage.setPhsIncomeAmount(15000);
        formPhsPage.clickPhsAddIncomeSfiButton();
        browser.sleep(500);

        formPhsPage.selectPhsIncomeModalNpRelationshipList('Self')
        formPhsPage.setPhsIncomeModalIncomeNpList('Consulting','5000');
        formPhsPage.setPhsIncomeModalIncomeNpList('Stipend','5000');
        formPhsPage.setPhsIncomeModalIncomeNpList('Per Diem','5000');
        expect(formPhsPage.getPhsIncomeModalNpAccumulatorTotal()).toEqual('$15,000.00');
        formPhsPage.setPhsIncomeModalNpBusinessPurposeStatement();
        expect(formPhsPage.getPhsIncomeModaNplBusinessPurposeWordCount())
            .toBe('1962 characters remaining');
        formPhsPage.selectPhsYesNoOption('npSfiRelated','YES');
        formPhsPage.setPhsIncomeModalNpBusinessReasonableStatement();
        expect(formPhsPage.getPhsIncomeModalNpReasonableWordCount())
            .toBe('1962 characters remaining');
        // formPhsPage.clickPhsIncomeModalNpCancelButton();
        formPhsPage.clickPhsIncomeModalNPSubmitButton();

        browser.sleep(500);
        formPhsPage.selectPhsYesNoOption('ipRights','NO');
        formPhsPage.selectPhsYesNoOption('travel','NO');
        formPhsPage.selectPhsYesNoOption('management','NO');
        formPhsPage.setPhsSignature();
        formPhsPage.clickPhsSubmitButton();
        browser.sleep(500);

     });
     it('should file a basic disclousre with just the non-public entity income and equity SFI - test #03b', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.selectPhsYesNoOption('completedTraining','YES');
        formPhsPage.selectPhsDisclosureType('Initial');
        formPhsPage.selectPhsEntity('TEST');
        expect(formPhsPage.getPhsEntityDropDownText()).toContain('TEST');
        formPhsPage.selectPhsEntityTypeList('Non-publicly traded entity');
        formPhsPage.setPhsIncomeAmount(15000);
        formPhsPage.selectPhsYesNoOption('equityInterest','YES');
        formPhsPage.setPhsIncomeEquityInterestAmount(25000);
        formPhsPage.clickPhsAddIncomeSfiButton();
        browser.sleep(500);

        formPhsPage.selectPhsIncomeModalNpRelationshipList('Self')
        formPhsPage.setPhsIncomeModalIncomeNpList('Dividends','5000');
        formPhsPage.setPhsIncomeModalIncomeNpList('Authorship','5000');
        formPhsPage.setPhsIncomeModalIncomeNpList('Honoraria','5000');
        expect(formPhsPage.getPhsIncomeModalNpAccumulatorTotal()).toEqual('$15,000.00');
        formPhsPage.setPhsIncomeModalNpBusinessPurposeStatement();
        expect(formPhsPage.getPhsIncomeModaNplBusinessPurposeWordCount())
            .toBe('1962 characters remaining');
        formPhsPage.selectPhsYesNoOption('npSfiRelated','YES');
        formPhsPage.setPhsIncomeModalNpBusinessReasonableStatement();
        expect(formPhsPage.getPhsIncomeModalNpReasonableWordCount())
            .toBe('1962 characters remaining');
        // formPhsPage.clickPhsIncomeModalNpCancelButton();
        formPhsPage.clickPhsIncomeModalNPSubmitButton();

        browser.sleep(500);
        formPhsPage.selectPhsYesNoOption('ipRights','NO');
        formPhsPage.selectPhsYesNoOption('travel','NO');
        formPhsPage.selectPhsYesNoOption('management','NO');
        formPhsPage.setPhsSignature();
        formPhsPage.clickPhsSubmitButton();
        browser.sleep(500);

     });

    it('should file a basic disclousre with an Intellectual Property SFI to file - test #04', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.selectPhsYesNoOption('completedTraining','NO');
        formPhsPage.selectPhsDisclosureType('Annual');
        formPhsPage.selectPhsYesNoOption('travel','NO');
        formPhsPage.selectPhsYesNoOption('management','NO');
        formPhsPage.selectPhsYesNoOption('ipRights','YES');
        formPhsPage.clickPhsAddSfi('ipRights');
        browser.sleep(250);

        formPhsPage.selectPhsIpModalEntityList('TEST');
        formPhsPage.selectPhsIpModalTotalAmountList('$5,001 - $9,999');
        formPhsPage.selectPhsIpModalRelationshipList('Dependent Children');
        formPhsPage.setPhsIpModalDescriptionOfIp();
        formPhsPage.selectPhsYesNoOption('ipSfiRelated','YES');
        formPhsPage.setPhsIpModalIpStatement();
        // formPhsPage.clickPhsIpModalCancelButton();
        formPhsPage.clickPhsIpModalSubmitButton();
        browser.sleep(750);

        formPhsPage.setPhsSignature();
        formPhsPage.clickPhsSubmitButton();
        browser.sleep(500);
    });

    it('should file a basic disclousre with an Travel   SFI to file - test #05', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.selectPhsYesNoOption('completedTraining','NO');
        formPhsPage.selectPhsDisclosureType('Annual');
        formPhsPage.selectPhsYesNoOption('ipRights','NO');

        formPhsPage.selectPhsYesNoOption('travel','YES');
        browser.sleep(300);
        formPhsPage.clickPhsAddSfi('travel');
        browser.sleep(300);

        formPhsPage.selectPhsTravelModalEntityList('TEST');
        formPhsPage.setPhsTravelModalDestinationTextbox();
        formPhsPage.setPhsTravelModalBeginTravelDate('01/31/2020');
        formPhsPage.setPhsTravelModalEndTravelDate('02/11/2020');
        formPhsPage.setPhsTravelModalReasonForTravelTextbox();
        browser.sleep(450);
        formPhsPage.selectPhsYesNoOption('travelSfiRelated','NO');
        formPhsPage.setPhsTravelModalTravelStatementTextbox();
        // formPhsPage.clickPhsTravelModalCancelButton();
        formPhsPage.clickPhsTravelModalSubmitButton();
        browser.sleep(1700);

        formPhsPage.selectPhsYesNoOption('management','NO');
        formPhsPage.setPhsSignature();
        formPhsPage.clickPhsSubmitButton();
        browser.sleep(500);
    });

    it('should file a basic disclousre with Outside Acitivity SFI to file - test #06', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.selectPhsYesNoOption('completedTraining','NO');
        formPhsPage.selectPhsDisclosureType('Annual');
        formPhsPage.selectPhsYesNoOption('ipRights','NO');
        formPhsPage.selectPhsYesNoOption('travel','NO');
        formPhsPage.selectPhsYesNoOption('management','YES');
        formPhsPage.clickPhsAddSfi('management');
        browser.sleep(250);

        formPhsPage.selecttPhsMgmtModalEntityList('Member Board of Directors');
        formPhsPage.setPhsMgmtModalResponsabilityStatementTextbox();;
        // formPhsPage.clickPhsMgmtModalCancelButton();
        formPhsPage.clickPhsManagementModalSubmitButton();
        browser.sleep(1300);

        formPhsPage.setPhsSignature();
        browser.sleep(250);
        formPhsPage.clickPhsSubmitButton();
    });

     it('should file a basic disclousre with just the non-public entity income and equity SFI in the $5000 range - test #07', function() {
        formPhsPage.navigateTo();
        formPhsPage.startPhsFiling();
        browser.sleep(250);

        formPhsPage.selectPhsYesNoOption('completedTraining','YES');
        formPhsPage.selectPhsDisclosureType('Correction');
        formPhsPage.selectPhsEntity('TEST');
        expect(formPhsPage.getPhsEntityDropDownText()).toContain('TEST');
        formPhsPage.selectPhsEntityTypeList('Non-publicly traded entity');
        formPhsPage.setPhsIncomeAmount(5000);
        formPhsPage.selectPhsYesNoOption('equityInterest','YES');
        formPhsPage.setPhsIncomeEquityInterestAmount(3000);
        formPhsPage.clickPhsAddIncomeSfiButton();
        browser.sleep(500);

        formPhsPage.selectPhsIncomeModalNpRelationshipList('Spouse/Registered Domestic Partner');
        expect(formPhsPage.isPhsIncomeModalNpAccumulatorTotalDisplayed()).toBe(false);
        formPhsPage.setPhsIncomeModalNpBusinessPurposeStatement();
        expect(formPhsPage.getPhsIncomeModaNplBusinessPurposeWordCount())
            .toBe('1962 characters remaining');
        formPhsPage.selectPhsYesNoOption('npSfiRelated','YES');
        formPhsPage.setPhsIncomeModalNpBusinessReasonableStatement();
        expect(formPhsPage.getPhsIncomeModalNpReasonableWordCount())
            .toBe('1962 characters remaining');
        // formPhsPage.clickPhsIncomeModalNpCancelButton();
        formPhsPage.clickPhsIncomeModalNPSubmitButton();

        browser.sleep(500);
        formPhsPage.selectPhsYesNoOption('ipRights','NO');
        formPhsPage.selectPhsYesNoOption('travel','NO');
        formPhsPage.selectPhsYesNoOption('management','NO');
        formPhsPage.setPhsSignature();
        formPhsPage.clickPhsSubmitButton();
        browser.sleep(500);

     });
});
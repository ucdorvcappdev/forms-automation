import { Form700uPage } from '../../pageObjects/forms700u.po';
import { browser } from 'protractor';
import { utils } from '../../utils/utils';

describe('Form 700-U Negative Tests', () => {
    let form700page:  Form700uPage;

    beforeEach(() => {
        form700page = new Form700uPage();
     });

    it('should cancel from creating form 700-U form - Test #01', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.clickOnf700CancelButton();
        expect(browser.getCurrentUrl()).toContain(browser.baseUrl + 'landing');
    });

    it('should check error messages when user saves an empty draft - Test #02', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.clickF700SaveDraftButton();

        expect(form700page.getf700ErrorDocNotSavedText()).toContain('Document not saved. Errors exist on page.');
        expect(form700page.getf700ErrorProjectText()).toContain('A research project is required');
        expect(form700page.getf700ErrorStatement()).toContain('Field is empty');
        expect(form700page.getf700ErrorEntity()).toContain('Select a funding entity');
        expect(form700page.getf700FundEntityResultText()).toContain('Not found in system');
        form700page.clickOnf700CancelButton();
        expect(browser.getCurrentUrl()).toContain(browser.baseUrl + 'landing');
    });

    it('should check error messages when user submits an incomplete form - Test #03', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        // form700page.clickF700SaveDraftButton();
        form700page.setf700Signature();
        form700page.clickFinalizeButton();

        // Verifying all top level errors
        expect(form700page.getf700ErrorDocNotSavedText()).toContain('Document not saved. Errors exist on page.');
        expect(form700page.getf700ErrorProjectText()).toContain('A research project is required');
        expect(form700page.getf700ErrorStatement()).toContain('Field is empty');
        expect(form700page.getf700ErrorEntity()).toContain('Select a funding entity');
        expect(form700page.getf700FundEntityResultText()).toContain('Not found in system');
        expect(form700page.getf700ErrorFundAmount()).toContain('Field is empty');
        expect(form700page.getf700ErrorEstimateQuestion()).toContain('Field is empty');
        expect(form700page.getf700ErrorHumanSubject()).toContain('Field is empty');
        expect(form700page.getf700ErrorAssociation()).toContain('Field is empty');
        expect(form700page.getf700ErrorInvestment()).toContain('Field is empty');
        expect(form700page.getf700ErrorIncome()).toContain('Field is empty');
        expect(form700page.getf700ErrorLoan()).toContain('Field is empty');
        expect(form700page.getf700ErrorGift()).toContain('Field is empty');
        expect(form700page.getf700ErrorTravelAmount()).toContain('Field is empty');

        form700page.clickOnf700CancelButton();
        expect(browser.getCurrentUrl()).toContain(browser.baseUrl + 'landing');
    });

    it('should veirfy correct error message when Association information is not complete - Test #04', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.selectf700YesNoOption('#association', 'YES');
        expect(form700page.getf700SupplementalFormRequiredMsgText()).toContain(browser.params.SFISupplementalMessage);
        form700page.setf700Signature();
        form700page.clickFinalizeButton();

        expect(form700page.getf700ErrorAssociation()).toContain('Field is empty');
        form700page.clickOnf700CancelButton();
        expect(browser.getCurrentUrl()).toContain(browser.baseUrl + 'landing');
    });

    it('should veirfy correct error message when Investment information is not complete - Test #05', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.selectf700YesNoOption('#thresholdInvestment', 'YES');
        expect(form700page.getf700SupplementalFormRequiredMsgText()).toContain(browser.params.SFISupplementalMessage);
        form700page.setf700Signature();
        form700page.clickFinalizeButton();

        expect(form700page.getf700ErrorInvestment()).toContain('Field is empty');
        form700page.clickOnf700CancelButton();
        expect(browser.getCurrentUrl()).toContain(browser.baseUrl + 'landing');
    });

    it('should veirfy correct error message when Income related information is not complete - Test #06', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.selectf700YesNoOption('#thresholdIncome', 'YES');
        expect(form700page.getf700SupplementalFormRequiredMsgText()).toContain(browser.params.SFISupplementalMessage);
        form700page.setf700Signature();
        form700page.clickFinalizeButton();

        expect(form700page.getf700ErrorIncome()).toContain('Field is empty');
        expect(form700page.getf700ErrorSpouseIncome()).toContain('Field is empty');
        form700page.clickOnf700CancelButton();
        expect(browser.getCurrentUrl()).toContain(browser.baseUrl + 'landing');
    });

    it('should veirfy correct error message when Loan related information is not complete - Test #07', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.selectf700YesNoOption('#thresholdLoan', 'YES');
        expect(form700page.getf700SupplementalFormRequiredMsgText()).toContain(browser.params.SFISupplementalMessage);
        form700page.setf700Signature();
        form700page.clickFinalizeButton();

        expect(form700page.getf700ErrorLoanAmount()).toContain('Field is empty');
        expect(form700page.getf700ErrorLoanType()).toContain('Field is empty');
        expect(form700page.getf700ErrorLoanRate()).toContain('Field is empty');
        expect(form700page.getf700ErrorLoanRepaid()).toContain('Field is empty');
        form700page.clickOnf700CancelButton();
        expect(browser.getCurrentUrl()).toContain(browser.baseUrl + 'landing');
    });


    it('should veirfy correct error messagw when gift related information is not complete - Test #08', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.selectf700YesNoOption('#thresholdGift', 'YES');
        expect(form700page.getf700SupplementalFormRequiredMsgText()).toContain(browser.params.SFISupplementalMessage);
        form700page.setf700Signature();
        form700page.clickFinalizeButton();

        expect(form700page.getf700ErrorGiftValue()).toContain('Field is empty');
        expect(form700page.getf700ErrorGiftDate()).toContain('Field is empty');
        form700page.clickOnf700CancelButton();
        expect(browser.getCurrentUrl()).toContain(browser.baseUrl + 'landing');
    });


    it('should veirfy correct error messagw when Travel related information is not complete - Test #09', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.selectf700YesNoOption('#paidTravel', 'YES');
        expect(form700page.getf700SupplementalFormRequiredMsgText()).toContain(browser.params.SFISupplementalMessage);
        form700page.setf700Signature();
        form700page.clickFinalizeButton();

        expect(form700page.getf700ErrorTravelPaymentAmount()).toContain('Field is empty');
        expect(form700page.getf700ErrorTravelAmount()).toContain('Field is empty');
        expect(form700page.getf700ErrorTravelDescription()).toContain('Field is empty');
        expect(form700page.getf700ErrorTravelStartDate()).toContain('Field is empty');
        expect(form700page.getf700ErrorTravelEndDate()).toContain('Field is empty');
        form700page.clickOnf700CancelButton();
        expect(browser.getCurrentUrl()).toContain(browser.baseUrl + 'landing');
    });





});
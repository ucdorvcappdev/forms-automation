import { Form700uPage } from '../../pageObjects/forms700u.po';
import { browser } from 'protractor';
// import { utils } from '../utils/utils';

describe('Create a basic Form 700-U Tests', () => {
    let form700page:  Form700uPage;
    // let util:   utils;

    beforeEach(() => {
        form700page = new Form700uPage();
        // util = new utils();
     });



    it('should create a basic form 700-U form (no conflicts reported)- Test #01', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.f700selectProject();
        form700page.f700setInitialNewFundingOption();
        form700page.f700setFundingDate();

        form700page.f700setSponsor('american cancer society');
        expect(form700page.getf700SponsorListText()).toContain('AMERICAN CANCER SOCIETY 8032');
        form700page.selectf700Sponsor('AMERICAN CANCER SOCIETY 8032');
        expect(form700page.getf700FundEntityResultText()).toContain('AMERICAN CANCER SOCIETY');
        form700page.setf700FundAmount(50000);
        expect(form700page.getf700FundAmountValue()).toContain(50000);
        form700page.selectf700YesNoOption('#estimated','NO');
        form700page.selectf700YesNoOption('#humanSubjects','NO');
        form700page.selectf700YesNoOption('#association','NO');
        form700page.selectf700YesNoOption('#thresholdInvestment','NO');
        form700page.selectf700YesNoOption('#thresholdIncome','NO');
        form700page.selectf700YesNoOption('#thresholdLoan','NO');
        form700page.selectf700YesNoOption('#thresholdGift','NO');
        form700page.selectf700YesNoOption('#paidTravel','NO');
        form700page.setf700Signature();
        // form700page.clickFinalizeButton();
    });

    it('should create a basic form 700-U form (with conflicts reported)- Test #02', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.f700selectProject();
        form700page.f700setInitialNewFundingOption();
        form700page.f700setFundingDate();
        // browser.sleep(500);

        form700page.f700setSponsor('american cancer society');
        expect(form700page.getf700SponsorListText()).toContain('AMERICAN CANCER SOCIETY 8032');
        form700page.selectf700Sponsor('AMERICAN CANCER SOCIETY 8032');
        expect(form700page.getf700FundEntityResultText()).toContain('AMERICAN CANCER SOCIETY');
        form700page.setf700FundAmount(50000);
        expect(form700page.getf700FundAmountValue()).toContain(50000);
        form700page.selectf700YesNoOption('#estimated','NO');
        form700page.selectf700YesNoOption('#humanSubjects','YES');
        form700page.selectf700YesNoOption('#association','YES');
        form700page.setf700AssociationText('partners');
        form700page.selectf700YesNoOption('#thresholdInvestment','NO');
        form700page.selectf700YesNoOption('#thresholdIncome','NO');
        form700page.selectf700YesNoOption('#thresholdLoan','NO');
        form700page.selectf700YesNoOption('#thresholdGift','NO');
        form700page.selectf700YesNoOption('#paidTravel','NO');

        expect(form700page.getf700AssociationValueText()).toContain('partner');
        expect(form700page.getf700SupplementalFormRequiredMsgText()).toContain(browser.params.Form700SupplementalMessage);
        browser.sleep(2000);
        form700page.setf700Signature();
        // form700page.clickFinalizeButton();

    });

    it('should create a basic form 700-U form that declares Investment Economic interest- Test #03', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.f700selectProject();
        form700page.f700setInitialNewFundingOption();
        form700page.f700setFundingDate();

        form700page.f700setSponsor('american red cross');
        expect(form700page.getf700SponsorListText()).toContain('AMERICAN RED CROSS 8084');
        form700page.selectf700Sponsor('AMERICAN RED CROSS 8084');
        expect(form700page.getf700FundEntityResultText()).toContain('AMERICAN RED CROSS');
        form700page.setf700FundAmount(50000);
        expect(form700page.getf700FundAmountValue()).toContain(50000);
        form700page.selectf700YesNoOption('#estimated','NO');
        form700page.selectf700YesNoOption('#humanSubjects','YES');
        form700page.selectf700YesNoOption('#thresholdInvestment','YES');
        form700page.clickOnOption('#investment2');
        form700page.selectf700YesNoOption('#association','NO');
        form700page.selectf700YesNoOption('#thresholdIncome','NO');
        form700page.selectf700YesNoOption('#thresholdLoan','NO');
        form700page.selectf700YesNoOption('#thresholdGift','NO');
        form700page.selectf700YesNoOption('#paidTravel','NO');

        expect(form700page.getf700SupplementalFormRequiredMsgText()).toContain(browser.params.Form700SupplementalMessage);
        browser.sleep(2000);

        form700page.setf700Signature();
        // form700page.clickFinalizeButton();
    });

    it('should create a basic form 700-U form that declares related Income information- Test #04', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.f700selectProject();
        form700page.f700setInitialNewFundingOption();
        form700page.f700setFundingDate();

        form700page.f700setSponsor('9162');
        expect(form700page.getf700SponsorListText()).toContain('STANFORD UNIVERSITY 9162');
        form700page.selectf700Sponsor('STANFORD UNIVERSITY 9162');
        expect(form700page.getf700FundEntityResultText()).toContain('STANFORD UNIVERSITY');
        form700page.setf700FundAmount(70000);
        expect(form700page.getf700FundAmountValue()).toContain(70000);
        form700page.selectf700YesNoOption('#estimated','NO');
        form700page.selectf700YesNoOption('#humanSubjects','NO');
        form700page.selectf700YesNoOption('#thresholdIncome','YES');
        form700page.clickOnOption('#income2');
        form700page.clickOnOption('#incomeThroughSpouse1');
        form700page.selectf700YesNoOption('#association','NO');
        form700page.selectf700YesNoOption('#thresholdInvestment','NO');
        form700page.selectf700YesNoOption('#thresholdLoan','NO');
        form700page.selectf700YesNoOption('#thresholdGift','NO');
        form700page.selectf700YesNoOption('#paidTravel','NO');

        expect(form700page.getf700SupplementalFormRequiredMsgText()).toContain(browser.params.Form700SupplementalMessage);

        form700page.setf700Signature();
        // form700page.clickFinalizeButton();
    });

    it('should create a basic form 700-U form that declares related Loan information- Test #05', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.f700selectProject();
        form700page.f700setInitialNewFundingOption();
        form700page.f700setFundingDate();

        form700page.f700setSponsor('6536');
        expect(form700page.getf700SponsorListText()).toContain('CALIFORNIA STATE UNIVERSITY 6536');
        form700page.selectf700Sponsor('CALIFORNIA STATE UNIVERSITY 6536');
        expect(form700page.getf700FundEntityResultText()).toContain('CALIFORNIA STATE UNIVERSITY');
        form700page.setf700FundAmount(70000);
        expect(form700page.getf700FundAmountValue()).toContain(70000);
        form700page.selectf700YesNoOption('#estimated','NO');
        form700page.selectf700YesNoOption('#humanSubjects','NO');
        form700page.selectf700YesNoOption('#association','NO');
        form700page.selectf700YesNoOption('#thresholdInvestment','NO');
        form700page.selectf700YesNoOption('#thresholdIncome','NO');
        form700page.selectf700YesNoOption('#thresholdLoan','YES');
        form700page.clickOnOption('#highestLoanBalance1');
        form700page.clickOnOption('#loanType2');
        form700page.clickOnOption('#loanRepaid2');
        form700page.setf700InterestRate(7.99);

        form700page.selectf700YesNoOption('#thresholdGift','NO');
        form700page.selectf700YesNoOption('#paidTravel','NO');

        expect(form700page.getf700SupplementalFormRequiredMsgText()).toContain(browser.params.Form700SupplementalMessage);
        browser.sleep(2000);

        form700page.setf700Signature();
        // form700page.clickFinalizeButton();
    });

    it('should create a basic form 700-U form that declares related GIFT information- Test #06', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.f700selectProject();
        form700page.f700setInitialNewFundingOption();
        form700page.f700setFundingDate();

        form700page.f700setSponsor('6161');
        expect(form700page.getf700SponsorListText()).toContain('HOKKAIDO UNIVERSITY 6161');
        form700page.selectf700Sponsor('HOKKAIDO UNIVERSITY 6161');
        expect(form700page.getf700FundEntityResultText()).toContain('HOKKAIDO UNIVERSITY');
        form700page.setf700FundAmount(100000);
        expect(form700page.getf700FundAmountValue()).toContain(100000);
        form700page.selectf700YesNoOption('#estimated','NO');
        form700page.selectf700YesNoOption('#humanSubjects','NO');
        form700page.selectf700YesNoOption('#association','NO');
        form700page.selectf700YesNoOption('#thresholdInvestment','NO');
        form700page.selectf700YesNoOption('#thresholdIncome','NO');
        form700page.selectf700YesNoOption('#thresholdLoan','NO');
        form700page.selectf700YesNoOption('#paidTravel','NO');
        form700page.selectf700YesNoOption('#thresholdGift','YES');
        form700page.setf700GiftDescription();
        form700page.setf700GiftValue(7500);
        form700page.setf700GiftDateReceived('09/19/2019');

        expect(form700page.getf700SupplementalFormRequiredMsgText()).toContain(browser.params.Form700SupplementalMessage);

        form700page.setf700Signature();
        form700page.clickFinalizeButton();
    });

    it('should create a basic form 700-U form that declares related paid TRAVEL information- Test #07', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        form700page.f700selectProject();
        form700page.f700setInitialNewFundingOption();
        form700page.f700setFundingDate();
        // browser.sleep(500);

        form700page.f700setSponsor('oxford');
        expect(form700page.getf700SponsorListText()).toContain('UNIVERSITY OF OXFORD 7838');
        form700page.selectf700Sponsor('UNIVERSITY OF OXFORD 7838');
        expect(form700page.getf700FundEntityResultText()).toContain('UNIVERSITY OF OXFORD');
        form700page.setf700FundAmount(45000);
        expect(form700page.getf700FundAmountValue()).toContain(45000);
        form700page.selectf700YesNoOption('#estimated','YES');
        form700page.selectf700YesNoOption('#humanSubjects','NO');

        form700page.selectf700YesNoOption('#association','NO');
        form700page.selectf700YesNoOption('#thresholdInvestment','NO');
        form700page.selectf700YesNoOption('#thresholdIncome','NO');
        form700page.selectf700YesNoOption('#thresholdLoan','NO');
        form700page.selectf700YesNoOption('#thresholdGift','NO');
        form700page.selectf700YesNoOption('#paidTravel','YES');
        form700page.clickOnOption('#paymentType1');
        form700page.setf700TravelPaidAmount(3550.22);
        form700page.setf700TravelDescription('Travel Description test - by Protractor automated tests');
        form700page.setf700TravelStartDate('06/01/2019');
        form700page.setf700TravelEndDate('06/15/2019');

        expect(form700page.getf700SupplementalFormRequiredMsgText()).toContain(browser.params.Form700SupplementalMessage);
        browser.sleep(2000);

        form700page.setf700Signature();
        // form700page.clickFinalizeButton();
    });


});
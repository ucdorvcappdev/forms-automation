import { Form700uPage } from '../../pageObjects/forms700u.po';
import { browser } from 'protractor';
import { EPERM } from 'constants';

describe('Form 700-U Verify base UI element Tests', () => {

    let form700page:  Form700uPage;

    beforeEach(() => {
        form700page = new Form700uPage();
     });

    it('should display form 700-U tab - Test #01', function() {
        form700page.navigateTo();
        expect(form700page.getForm700Text()).toMatch('Form 700-U Submissions');
    });

    it('should verify form 700-U table headers - Test #02', function() {
        form700page.navigateTo();
        expect(form700page.getf700TableHeaders()).toEqual( ['Confirmation', 'Signature', 'Date Submitted', 'Funding Amount', 'Project', 'Funding Entity (user added entities in green)', 'Status', 'Supplemental Form', '⇣ PDF'] );
    });

    it('should verify Copyright statement on form 700-U - Test #03', function() {
        form700page.navigateTo();
        expect(form700page.getf700CopyrightText()).toContain('Copyright © The Regents of the University of California, Davis campus. All rights reserved. Questions? Contact us at or_coi@ad3.ucdavis.edu');
    });

    it('should verify footer on form 700-U - Test #04', function() {
        form700page.navigateTo();
        expect(form700page.getf700FooterText()).toContain('Form 700-U: Statement of Economic Interests for Principal Investigators | Form 800: Statement of Economic Interest | Supplemental: Supplemental to Statement of Economic Interest | PHS: Public Health Service Disclosure');
    });

    it('should verify no form 700-U submitted message - Test #05', function() {
        form700page.navigateTo();
        expect(form700page.getNoF700MsgText()).toContain('You have no Form 700-U\'s submitted. Note: this form is only required for those who are designated the Principal Investigators on a project.');
    });

    it('should verify form 700-U create form UI elements - Test #06', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();
        expect(form700page.getf700PageHeaderText()).toContain('Form 700-U - Statement of Economic Interest');
    });

    it('should verify the Demographics field labels - Test #07', function () {
        // form700page.navigateTo();
        expect(form700page.getf700LegendText()).toContain('Demographics');
        expect(form700page.getf700NameLabelText()).toMatch('Name');
        expect(form700page.getf700EmailLabelText()).toMatch('Email');
        expect(form700page.getf700DeptLabelText()).toMatch('Department');
    });

    it('should verify the Demographics value - Test #08', function () {
        // form700page.navigateTo();
        expect(form700page.getf700UsernameText()).toMatch('OR Tester');
        expect(form700page.getf700EmailText()).toMatch('trholmes@ucdavis.edu');
        expect(form700page.getf700DeptText()).toMatch('INFO SYSTEMS & SVCS - RESEARCH');
    });

    it('should verify the Funding Details labels - Test #09', function() {
        expect(form700page.getf700FundingDetailsLabelText()).toContain('1 & 2. Funding Details:Before proceeding Click here to confirm that your sponsor is not exempt from the 700-U.');
        expect(form700page.getf700ResearchLabelText()).toContain('Research Project');
        expect(form700page.isNoProjectLinkDisplayed()).toBe(true);
        expect(form700page.getNoProjectLinkLabelText()).toContain('Can\'t find your project?');

        expect(form700page.getf700StatementTypeLabelText()).toContain('Type of Statement');
        expect(form700page.getf700StatementTypesText()).toContain('Initial (for new funding)\nInterim (for renewed funding)');
        expect(form700page.getf700FundingDateLabelText()).toContain('Funding Date\n(MM/DD/YYYY)');
        expect(form700page.getf700FundingDatePlaceholderText()).toContain('Date of initial or renewed funding');
        expect(form700page.getf700EntitySearchLabelText()).toContain('Funding Entity Search');
        expect(form700page.getf700EntitySearchPlaceHolderText()).toContain('search sponsor name or code');
        expect(form700page.getf700FundEntityResultLabelText()).toContain('Funding Entity');
        expect(form700page.isf700FundEntityResultDisplayed()).toBe(false);
        expect(form700page.getf700FundAmountLabelText()).toContain('Funding Amount');
        expect(form700page.getf700FundAmountContent()).toBe('');
        expect(form700page.getf700EstimateLabelText()).toContain('Is this amount an estimate?');
        expect(form700page.getf700EstimateYesText()).toContain('YES');
        expect(form700page.getf700EstimateNoText()).toContain('NO');
        expect(form700page.getf700HumanSubjectLabelText()).toContain('Are Human Subjects Involved?');
        expect(form700page.getf700HumanSubjectYesOption()).toContain('YES');
        expect(form700page.getf700HumanSubjectNoOption()).toContain('NO');
        expect(form700page.getf700FilerLegendLabelText()).toContain('3. Filer Information');
    });


    it('should verify the Filer Information labels - Test #10', function() {
        expect(form700page.getf700AssociationLabelText()).toContain('A. Your Association');
        expect(form700page.getf700AssociationLabelText()).toContain('Association');
        expect(form700page.getf700AssociationTitleLabelText()).toContain('Title');
        expect(form700page.getf700AssociationTitlePlaceholderText()).toContain('If answered yes above');
        expect(form700page.getf700AssocHelpText()).toContain(browser.params.AssociationHelp);

        expect(form700page.getf700InvestmentTitleLabelText()).toContain('B. Your Related Investments');
        expect(form700page.getf700InvestmentTitleLabelText()).toContain('Investment');
        expect(form700page.getf700InvestHelpText()).toContain(browser.params.InvestmentHelp);
        expect(form700page.isf700InvestmentYesOptionDisplayed()).toBe(true);
        expect(form700page.isf700InvestmentNoOptionDisplayed()).toBe(true);

        expect(form700page.getf700IncomeTitleLabelText()).toContain('C. Your Related Income');
        expect(form700page.getf700IncomeTitleLabelText()).toContain('Income');
        expect(form700page.getf700IncomeHelpText()).toContain(browser.params.IncomeHelp);
        expect(form700page.isf700IncomeYesOptionDisplayed()).toBe(true);
        expect(form700page.isf700IncomeNoOptionDisplayed()).toBe(true);

        expect(form700page.getf700LoanTitleLabelText()).toContain('D. Your Related Loan');
        expect(form700page.getf700LoanTitleLabelText()).toContain('Loan Information');
        expect(form700page.getf700LoanHelpText()).toContain(browser.params.LoanInfoHelp);
        expect(form700page.isf700LoanIncomeYesOptionDisplayed()).toBe(true);
        expect(form700page.isf700LoanIncomeNoOptionDisplayed()).toBe(true);

        expect(form700page.getf700GiftsTitleLabelText()).toContain('E. Your Related Gifts');
        expect(form700page.getf700GiftsTitleLabelText()).toContain('Gift Information');
        expect(form700page.getf700GiftsHelpText()).toContain(browser.params.RelatedGiftsHelp);
        expect(form700page.isf700GiftsYesOptionDisplayed()).toBe(true);
        expect(form700page.isf700GiftsNoOptionDisplayed()).toBe(true);

        expect(form700page.getf700TravelTitleLabelText()).toContain('F. Your Related Paid Travel');
        expect(form700page.getf700TravelTitleLabelText()).toContain('Travel Information');
        expect(form700page.getf700TravelHelpText()).toContain(browser.params.RelatedTravelHelp);
        expect(form700page.isf700TravelYesOptionDisplayed()).toBe(true);
        expect(form700page.isf700TravelNoOptionDisplayed()).toBe(true);

        expect(form700page.getf700VerificationHelpText()).toContain(browser.params.VerificationSignatureStatement);
    });

    it('should verify all hidden sub UI items of each section on form 700-U - Test #11', function() {
        form700page.navigateTo();
        form700page.clickOnCreateForm700();

        // Sets all sections to YES to verify hidden UI labels and textboxes
        form700page.selectf700YesNoOption('#estimated','YES');
        form700page.selectf700YesNoOption('#humanSubjects','YES');
        form700page.selectf700YesNoOption('#association','YES');
        form700page.selectf700YesNoOption('#thresholdInvestment','YES');
        form700page.selectf700YesNoOption('#thresholdIncome','YES');
        form700page.selectf700YesNoOption('#thresholdLoan','YES');
        form700page.selectf700YesNoOption('#thresholdGift','YES');
        form700page.selectf700YesNoOption('#paidTravel','YES');

        //UI Verification of items under each section
        expect(form700page.getf700InvestmentValueLabel()).toContain('Value is');
        expect(form700page.getf700InvestmentDateDisposedLabel()).toContain('Date Disposed (if applicable)');
        // browser.sleep(5000);
    });
});



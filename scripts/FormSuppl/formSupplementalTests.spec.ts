import { FormSuppPage } from '../../pageObjects/formsSupplemental.po';
import { browser } from 'protractor';

describe('Form Supplemental Tests', () => {

    let formSuppPage:  FormSuppPage;

    beforeEach(() => {
        formSuppPage = new FormSuppPage();
     });

    it('should display form PHS tab - test #01', function() {
        formSuppPage.navigateTo();
        expect(formSuppPage.getFormSuppText()).toContain('Economic Interest - Supplement');
    });

    it('should verify form PHS table headers - test #02', function() {
        formSuppPage.navigateTo();
        expect(formSuppPage.getSuppTableHeaderText()).toContain('Investigator Signature Date\nSubmitted Project Funding Entity (user added entities in green) Status PDF','failed to match string');
    });

    it('should verify form PHS no submitted form message - test #03', function() {
        formSuppPage.navigateTo();
        expect(formSuppPage.getNoSuppFormSubmittedText()).toContain('You have no Supplement forms submitted.');
    });
});
